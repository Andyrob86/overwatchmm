<?php 
$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"/overwatchmm/www":"";
include_once "{$root}/../vendor/autoload.php";
set_debug();
if ( is_session_started() === FALSE ) {
	$session = new session();
	$session->start_session('_s', true);
}
include "header.php";
?>
<body>	
	<div class="container-fluid">
		<div class="spacer-md"></div>
		<div class="col-sm-8 col-sm-offset-2 col-xs-12  main-cont">
			<!-- Navigation -->
			<?php include 'nav.php'; ?>
			<div class="spacer-md"></div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<h1>Oh No!!</h1>
					<strong><h3>We're terribly sorry, but something went wrong while using OverwatchMM! The only thing we can do now, is start over and try again :(</h3><h3>If this keeps happening, please don't hesitate to contact us!</h3></strong>
					<div class="spacer-md"></div>
					<button class='btn-lg btn-warning' id='reset_button'><strong>Start Over</strong></button>
					<div class="spacer-sm"></div>
				</div>
			</div>
			<?php include 'footer.php'; ?>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class='spacer-lg'></div>
			</div>
		</div>
	</div>
<script type="text/javascript">
$(document).ready(function(){
	$("#reset_button").click(function(e){
		e.preventDefault();
		$.ajax({
			 type: "POST",
			 url: 'uhandler.php',
			 data: {"reset":1},
			 success: function(data)
			 {
				 console.log("data: " + data);
				 var response = $.parseJSON(data);
				 var info = undefined;
				if(response.state == "success"){
					location.reload();
				}
			 }
		 });
	})
})
</script>
</body>
</html>