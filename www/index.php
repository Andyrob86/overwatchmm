<?php
$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"/overwatchmm/www":"";
include_once "{$root}/../vendor/autoload.php";
use Respect\Validation\Validator as v;
if (isset($_GET['l']) && $_GET['l'] != ""){
	if (v::alnum()->length(6,10)->validate($_GET['l'])){
		$l = $_GET['l'];
		$link = substr($l, -6);
		$id = str_replace($link,"",$l);
		$db = new Database();
		$db->query("SELECT `group` FROM `cg_shortlinks` WHERE `id` = :id AND `link` = :l");
		$db->bind(":id",$id);
		$db->bind(":l",$link);
		if ($result = $db->single()){
			$group = $result['group'];
			if(isset($db))$db->destroy();
			header("location: {$httproot}/groupq?g={$group}&l={$l}");
		}
	} else {
		header("location: {$httproot}/groupq?g=notfound");
	}
}
set_debug();
if ( is_session_started() === FALSE ) {
	$session = new session();
	$session->start_session('_s', true);
}
$pagetitle = "Overwatch Match Maker";
include "header.php";
?>
<body>
    <div class="container-fluid">
			<div class="spacer-md hidden-xs"></div>
			<div class="row">
					<div id="main-cont" class="col-sm-8 col-sm-offset-2 col-xs-12 main-cont">
						<?php include "nav.php";  ?>
						<div class="spacer-md"></div>
						<div class="row" id="title_row">
							<div class="col-sm-12 text-center">
								<h1>Welcome to Overwatch Match Maker!</h1>
								<h3><strong>Select your platform to begin looking for a group.</strong></h3>
								<div class="spacer-sm"></div>
								<hr class="hr to-edge">
								<div class="spacer"></div>
							</div>
						</div>
						<div class="row messages_row" id="messages_row">
							<div class="col-sm-12">
								<div id="messages"></div>
							</div>
						</div>
						<?php 
						if(isset($_SESSION['data'])){
							if(!isset($_SESSION['data']['group'])) join_form($_SESSION['data']); 
						} else {
							join_form();
						};
						?>
						<div class="row" id="main_disp_row">
							<div class="col-xs-12 text-center" id="main_disp">
								<?php create_player_table(); ?>
							</div>
						</div>
						
						<div class="row chat_row" id="chat_row">
							<div class="col-xs-12 col-md-10 col-md-offset-1">
								<div class="row">
									<div class="col-xs-12">
										<div class="chat-win" id="chat-win"></div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
											<div class="input-group">
												<label for="chat-input" class="sr-only">Message</label>
												<textarea class="form-control chat-input" name="chat-input" id="chat-input" placeholder="Type your message"></textarea>
												<span class="input-group-btn">
													<button type="submit" id="chat-btn" class="btn btn-default btn-xs btn-block chat-btn">Send</button>
												</span>
											</div>
									</div>
								</div>
								
							</div>
						</div>
						<?php 
						modals("all");
						include "footer.php"; 
						?>
						<script type='text/javascript'>var http_root = '<?php echo $httproot ?>';
						$( document ).ready(function() {
							if (typeof info != 'undefined'){
								if (info.group != null || ""){
									get_groupData();
								}
							} else {
								$.ajax({
								 type: "POST",
								 url: 'ajaxdata.php',
								 data: {"get_data":1},
								 success: function(data)
								 {
									 if (data != ""){
										info = $.parseJSON(data);
										if(info.group != null || ""){
											get_groupData();
										 }
									}
								 }
							 });
							}
						});
						</script> 
					</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class='spacer-lg'></div>
				</div>
			</div>
		</div>
	</body>
</html>
<?php

?>