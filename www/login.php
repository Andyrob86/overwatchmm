<?php 
$pagetitle = "Overwatch Match Maker";
$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"/overwatchmm/www":"";
include_once "{$root}/../vendor/autoload.php";
set_debug();
if ( is_session_started() === FALSE ) {
	$session = new session();
	$session->start_session('_s', true);
}
use Respect\Validation\Validator as v;
if(isset($_POST['email']) && v::email()->validate($_POST['email'])){
	$db = new Database();
	$db->query("SELECT salt FROM `access_list` where `email` = :e");
	$db->bind(":e",$_POST['email']);
	if($result = $db->single()){
		$rdata = array();
		$_SESSION['allowed']['u'] = $_POST['email'];
		$_SESSION['allowed']['salt'] = $result['salt'];
		$rdata['state'] = "success";
		$rdata['message'] = "<h1>Access Granted!</h1><h3><strong><a style='color:white' href='".(($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"/overwatchmm/www":"")."/'>Click here to start using Overwatch Match Maker!!</a></strong></h3>";
		echo json_encode($rdata);
	}
	
	exit();
}
include "header.php";
?>
<body>
	<div class="container-fluid">
		<div class="spacer-md hidden-xs"></div>
		
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 col-xs-12  main-cont">
			<?php 
			login_form();
			include 'footer.php'; 
			?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class='spacer-lg'></div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#login").submit(function(e){
				e.preventDefault();
				$.ajax({
					 type: "POST",
					 url: 'login.php',
					 data: {"email":$("#email").val()},
					 success: function(data)
					 {
						 console.log("data: " + data);
						 var response = $.parseJSON(data);
						 var info = undefined;
						if(response.state == "success"){
							$("#messages").html(response.message);
						}
					 }
				 });
			})
		})
	</script>
</body>
</html>