<?php
$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"/overwatchmm/www":"";
include_once "{$root}/../vendor/autoload.php";
use Respect\Validation\Validator as v;
if(!isset($db)){
	$db = new Database();
}
set_debug();
if ( is_session_started() === FALSE ) {
	$session = new session();
	$session->start_session('_s', true);
}
$pagetitle = "Overwatch Match Maker - Custom Group";
$hideregion = true;
include "header.php";



if (isset($_SESSION['cust']['group'])){
?>
<body>
    <div class="container-fluid">
			<div class="spacer-md hidden-xs"></div>
			<div class="row">
					<div id="main-cont" class="col-sm-8 col-sm-offset-2 col-xs-12 main-cont">
						<?php include "nav.php";  ?>
						<div class="spacer-md"></div>
						<div class="row" id="title_row">
							<div class="col-sm-12 text-center">
								<h1>Group Queue</h1>
								<h3><strong>Use the link below to invite your friends to this queue!</strong></h3>
								<p class='cust-invite-link'>https://overwatchmm.com/?l=<?php echo $_SESSION['cust']['link']?></p>
								<div class="spacer-sm"></div>
								
								<hr class="hr to-edge">
								<div class="spacer"></div>
							</div>
						</div>
						<div class="row messages_row" id="messages_row">
							<div class="col-sm-12">
								<div id="messages">
								<?php 
								if (isset($_GET['g'])){
									if($_GET['g'] != $_SESSION['cust']['group']){
										echo "<div class='alert alert-warning fade in out'><a href='#' class='close' data-dismiss='alert' aria-label='close'><i class='fa fa-times-circle fa-lg fa-fw' aria-hidden='true'></i></a>To join a different queue, you must first leave this queue!<div>";
									}
								}
								?>
								</div>
							</div>
						</div>
						<div class="row" id="main_disp_row">
							<div class="col-xs-12">
								<span id='user-controls' class='user-controls'><button type="button" class='btn btn-warning' id='leave_cust_q' >Leave Queue</button></span>
							</div>
							<div class="col-xs-12" id="main_disp">
								<?php create_cust_table(); ?>
							</div>
						</div>
						<?php 
						modals("all");
						include "footer.php"; 
						?>
						<script src="https://rubaxa.github.io/Sortable/Sortable.js"></script>
						<script type='text/javascript'>
						var http_root = '<?php echo $httproot ?>';
						<?php if (isset($_SESSION['cust'])){ $json = json_encode($_SESSION['cust']); echo "info = {$json};";} ?>
						$( document ).ready(function() {
							if (typeof info != 'undefined'){
								if (info.group != null || ""){
									get_custData();
								}
							}
						});
						changed = true;
						$('#modalSmallFooter').on('click', '#leave_cust_q_confirm', function () {
							$.ajax({
								type:"POST",
								url:"custqhandler.php",
								data:{"leave_queue":1},
								success: function(data){
									var response = $.parseJSON(data);
									if (response.state == "success"){
										window.location.href = "https://overwatchmm.com/groupq";
									}
									if (response.state == "error"){
										
									}
								}
							})
						});
						
						$('#modalSmallFooter').on('click', '#kick_cust_q_confirm', function () {
							$.ajax({
								type:"POST",
								url:"custqhandler.php",
								data:{"queue-kick":1,"player":$(this).attr("player")},
								success: function(data){
									var response = $.parseJSON(data);
									if (response.state == "success"){
									}
									if (response.state == "error"){
										
									}
								}
							})
						});
						
						$('#modalSmallFooter').on('click', '#remove_cust_q_confirm', function () {
							$.ajax({
								type:"POST",
								url:"custqhandler.php",
								data:{"queue-remove":1,"player":$(this).attr("player")},
								success: function(data){
									var response = $.parseJSON(data);
									if (response.state == "success"){
									}
									if (response.state == "error"){
										
									}
								}
							})
						});
						
						$('#modalSmallFooter').on('click', '#cycle_cust_q_confirm', function () {
							$.ajax({
								type:"POST",
								url:"custqhandler.php",
								data:{"cycle-queue":1},
								success: function(data){
									var response = $.parseJSON(data);
									if (response.state == "success"){
									}
									if (response.state == "error"){
										
									}
								}
							})
						});
						
						$('.modal-footer').on('click', '#confirm_move_lead', function () {
							$.ajax({
								type:"POST",
								url:"custqhandler.php",
								data:{"alter-queue":1,"leadtransfer":1,"oldpos":$(this).attr("oldpos"),"newpos":$(this).attr("newpos")},
								success: function(data){
									var response = $.parseJSON(data);
									if (response.state == "success"){
										location.reload();
									}
									if (response.state == "error"){
									}
								}
							})
						});
						
						function restart_timer(){
							timer = setTimeout( get_custData, 3000 );
						}
						
						function reset_sort(){
							$("#players_list").html("<span id='refresh-notice'>Refreshing List <i class='fa fa-spinner fa-pulse fa-fw fa-2x wobble-fix'></i></span>");
							restart_timer();
						}
						
						function update_sort(){
							var container = document.getElementById("players_list");
							var sort = Sortable.create(container, {
								group: "players",
								animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
								draggable: ".player-listing", // Specifies which items inside the element should be sortable
								scroll: true, // or HTMLElement
								scrollSensitivity: 30, // px, how near the mouse must be to an edge to start scrolling.
								scrollSpeed: 10,
								onStart: function (/**Event*/evt){
									clearTimeout(timer);
								},
								onEnd: function (/**Event*/evt) {
									if(evt.oldIndex == 0 && evt.newIndex != 0 || evt.newIndex == 0 && evt.oldIndex != 0){
										console.log(evt.oldIndex);
										console.log(evt.newIndex);
										
										$(".modal-title").html("Warning");
										$(".modal-body").html("You are about to transfer leadership of group, do you want to continue?");
										$(".modal-footer").html("<button type='button' class='btn btn-default' onclick='reset_sort()' data-dismiss='modal'>Cancel</button><button type='button' id='confirm_move_lead' oldpos='"+evt.oldIndex+"' newpos='"+evt.newIndex+"' leadtransfer='1' class='btn btn-success' data-dismiss='modal'>Confirm</button>")
										$("#modal-small").modal("show");
									} else {
										if (evt.oldIndex != 0 && evt.newIndex != 0 && evt.oldIndex != evt.newIndex && typeof evt.newIndex != 'undefined'){
											$.ajax({
												type:"POST",
												url:"custqhandler.php",
												data:{"alter-queue":1,"oldpos":evt.oldIndex,"newpos":evt.newIndex},
												success: function(data){
													var response = $.parseJSON(data);
													if (response.state == "success"){
														reset_sort()
													}
													if (response.state == "error"){
														$(".modal-title").html("Error");
														$(".modal-body").html("There was an answer changing group order!");
														$(".modal-footer").html("<button type='button' class='btn btn-default' data-dismiss='modal'>Ok</button>")
														$("#modal-small").modal("show");
														reset_sort()
													}
												}
											})
										} else {
											restart_timer()
										}
									}
								}
							});
						}
						
						$('#leave_cust_q').click(function(){
							$(".modal-title").html("Leave Group");
							$(".modal-body").html("Are you sure you want to leave this queue?");
							if(!$('#leave_cust_q_confirm').length)$(".modal-footer").html("<button type='button' class='btn btn-default' data-dismiss='modal'>Cancel</button><button type='button' id='leave_cust_q_confirm' class='btn btn-success'>Confirm</button>");
							$("#modal-small").modal("show");
						});
						
						$('#user-controls').on('click', "#cycle_cust_q",  function(){
							$(".modal-title").html("Cycle Queue");
							$(".modal-body").html("Are you sure you want to cycle the queue?");
							$(".modal-footer").html("<button type='button' class='btn btn-default' data-dismiss='modal'>Cancel</button><button type='button' id='cycle_cust_q_confirm' class='btn btn-success' data-dismiss='modal'>Confirm</button>");
							$("#modal-small").modal("show");
						});
						
						$('#user-controls').on('click', "#cust_q_help", function(){
							$(".modal-title").html("Help");
							$(".modal-body").html("Leader Control Help<ul class='list-group'><li class='list-group-item'><h3 class='list-group-item-heading'>Rearrange player order</h3><p class='list-group-item-text'>To rearrange members in this queue, click and drag them to a new position. Dragging yourself from the top position will make player 2 leader. Dragging someone into the top position will make them the leader.</p></li><li class='list-group-item'><h3 class='list-group-item-heading'>Cycle Queue</h3><p class='list-group-item-text'><div class='pull-left'><button type='button' class='btn btn-primary' ><i class='fa fa-2x fa-recycle'></i></button></div>Clicking this button will take the player in position 2 of the group, and move the to the end of the queue.</p></li><li class='list-group-item'><h3 class='list-group-item-heading'>Remove Player</h3><p class='list-group-item-text'><div class='pull-left'><button type='button' class='btn btn-info' ><i class='fa fa-2x fa-sign-out'></i></button></div>Clicking this button will remove the player from the queue. The player will be able to rejoin the queue at any point in time.</p></li><li class='list-group-item'><h3 class='list-group-item-heading'>Kick Player</h3><p class='list-group-item-text'><div class='pull-left'><button type='button' class='btn btn-danger' ><i class='fa fa-2x fa-ban'></i></button></div>Clicking this button will kick the player from the queue. They will NOT be able to rejoin the queue!</p></li></ul>");
							$(".modal-footer").html("<button type='button' class='btn btn-default' data-dismiss='modal'>Ok</button>");
							$("#modal-large").modal("show");
						});
						
						$("#players_list").on("click",".queue-kick",function(){
							var kickplayer = $(this).attr("player");
							$(".modal-title").html("Kick Player");
							$(".modal-body").html("Are you sure you want to kick "+kickplayer+"?");
							$("#modalSmallCloseBtn").html("Cancel");
							if(!$('#kick_cust_q_confirm').length)$(".modal-footer").html("<button type='button' class='btn btn-default' data-dismiss='modal'>Cancel</button><button type='button' id='kick_cust_q_confirm' player='"+kickplayer+"' data-dismiss='modal' class='btn btn-success'>Confirm</button>");
							$("#modal-small").modal("show");
							
						})
						
						$("#players_list").on("click",".queue-remove",function(){
							var removeplayer = $(this).attr("player");
							$(".modal-title").html("Remove Player");
							$(".modal-body").html("Are you sure you want to remove "+removeplayer+"?");
							$("#modalSmallCloseBtn").html("Cancel");
							if(!$('#remove_cust_q_confirm').length)$(".modal-footer").html("<button type='button' class='btn btn-default' data-dismiss='modal'>Cancel</button><button type='button' id='remove_cust_q_confirm' player='"+removeplayer+"' data-dismiss='modal' class='btn btn-success'>Confirm</button>");
							$("#modal-small").modal("show");
							
						})
						
						function update_player_list(data){
							changed = false;
							var pcount = 0;
							$.each(data.kicked, function(index, player){
								if (player == info.name){
									$.ajax({
										type:"POST",
										url:"custqhandler.php",
										data:{"kicked":1,"player":player},
										success: function(data){
											var response = $.parseJSON(data);
											if (response.state == "success"){
												$(".modal-title").html("Kicked");
												$(".modal-body").html(response.mbody);
												$(".modal-footer").html("<button type='button' class='btn btn-default' onclick='location.reload()' data-dismiss='modal'>Ok</button>")
												$("#modal-small").modal("show");
											}
											if (response.state == "error"){
												
											}
										}
									})
								}
							});
							if (data.players[0] == info.name){
								if(!$("#cycle_cust_q").length)$("#user-controls").append("<button type='button' class='btn btn-primary' id='cycle_cust_q' ><i class='fa fa-2x fa-recycle'></i></button>");
								if(!$("#cust_q_help").length)$("#user-controls").append("<button type='button' class='btn btn-info' id='cust_q_help' ><i class='fa fa-2x fa-question'></i></button>");
								changed = false;
								$.each(data.players, function( player_index, value ) {
									if(pcount < player_index) pcount = player_index;
									if(player_index <= 5){
										var classes = "list-group-item player-listing in-game";
									} else {
										var classes = "list-group-item player-listing in-queue";
									}
									if (player_index == 0){
										var rownum = "Leader: ";
									} else {
										var rownum = (player_index+1)+". ";
									}
									
									if (info.name == value){
										var classes = classes + " active"
										if($("#p"+player_index+"div").length){
											if ($("#p"+player_index+"title").html() != value){
												changed = true;
												$("#p"+player_index+"title").html(value);
												$("#p"+player_index+"div").addClass("active");
												if ($("#p"+player_index+"text").html() != ""){
													$("#p"+player_index+"text").html("");
												}
											}
										} else {
											changed = true;
											$("#players_list").append("<li class='"+classes+"' prow='"+player_index+"' id='p"+player_index+"div'><span class='list-group-item-heading'><span class='player-pos' id='p"+player_index+"pos'>"+rownum+"</span><span class='player-title' id='p"+player_index+"title'>"+value+"</span><span id='p"+player_index+"text' class='list-group-item-text'></span></li>");
										}
									} else {
										if($("#p"+player_index+"div").length){
											
											if ($("#p"+player_index+"title").html() != value){
												changed = true;
												$("#p"+player_index+"title").html(value);
												$("#p"+player_index+"div").removeClass("active");
												$("#p"+player_index+"text").html(" -- <button type='button' class='btn btn-info queue-remove' player='"+value+"'><i class='fa fa-sign-out'></i></button><button type='button' class='btn btn-danger queue-kick' player='"+value+"'><i class='fa fa-ban'></i></button>");
											}
											if ($("#p"+player_index+"text").html() == ""){
												$("#p"+player_index+"text").html(" -- <button type='button' class='btn btn-info queue-remove' player='"+value+"'><i class='fa fa-sign-out'></i></button><button type='button' class='btn btn-danger queue-kick' player='"+value+"'><i class='fa fa-ban'></i></button>");
											}
										} else {
											changed = true;
											$("#players_list").append("<li class='"+classes+"' prow='"+player_index+"' id='p"+player_index+"div'><span class='list-group-item-heading' ><span class='player-pos' id='p"+player_index+"pos'>"+rownum+"</span><span id='p"+player_index+"title'>"+value+"</span><span id='p"+player_index+"text' class='list-group-item-text'></span></li>");
											$("#p"+player_index+"text").html(" -- <button type='button' class='btn btn-info queue-remove' player='"+value+"'><i class='fa fa-sign-out'></i></button><button type='button' class='btn btn-danger queue-kick' player='"+value+"'><i class='fa fa-ban'></i></button>");
										}
									}
								})
								update_sort()
							} else {
								if($("#cycle_cust_q").length)$("#user-controls").remove();
								if($("#cust_q_help").length)$("#user-controls").remove();
								$.each(data.players, function( player_index, value ) {
									if(pcount < player_index) pcount = player_index;
									if(player_index <= 5){
										var classes = "list-group-item player-listing in-game";
									} else {
										var classes = "list-group-item player-listing in-queue";
									}
									
									if (player_index == 0){
										var rownum = "Leader: ";
									} else {
										var rownum = (player_index+1)+". ";
									}
									
									if (info.name == value){
										var classes = classes + " active"
										if($("#p"+player_index+"div").length){
											
											if ($("#p"+player_index+"title").html() != value){
												$("#p"+player_index+"title").html(value);
												$("#p"+player_index+"div").addClass("active");
											}
											$("#p"+player_index+"div").addClass("active");
										} else {
											$("#players_list").append("<li class='"+classes+" prow='"+player_index+"' id='p"+player_index+"div'><span class='list-group-item-heading' ><span class='player-pos' id='p"+player_index+"pos'>"+rownum+"</span><span id='p"+player_index+"title'>"+value+"</span><span id='p"+player_index+"text' class='list-group-item-text'></span></li>");
										}
									} else {
										if($("#p"+player_index+"div").length){
											
											if ($("#p"+player_index+"title").html() != value){
												$("#p"+player_index+"title").html(value);
												$("#p"+player_index+"div").removeClass("active");
											}
										} else {
											$("#players_list").append("<li class='"+classes+"' prow='"+player_index+"' id='p"+player_index+"div'><span class='list-group-item-heading' ><span class='player-pos' id='p"+player_index+"pos'>"+rownum+"</span><span id='p"+player_index+"title'>"+value+"</span><span id='p"+player_index+"text' class='list-group-item-text'></span></li>");
										}
									}
								})
							}
							i = 1;
							pos = "";
							$(".player-listing").each(function(){
								if (i == 1){
									pos = "Leader: ";
								}else{
									pos = i+". ";
								}
								$(this).find(".player-pos").html(pos);
								if($(this).attr("prow") > pcount){
									$(this).remove();
									changed = true;
								}
								i++;
							});
							$(".player-listing").children(".player-pos").html(pos);
						}
						
						function get_custData() {
							if($("#refresh-notice").length > 0)$("#refresh-notice").remove();
							if($("#reg_ul").is(":visible"))$("#reg_ul").hide();
							if(!$("#messages_row").is(":visible"))$("#messages_row").show();
							if (typeof info != 'undefined'){
								$.getJSON( http_root + "/group_manager/" + info.group + ".json", function( data ) {
									update_player_list(data);
								 }, 'json');
								timer = setTimeout( get_custData, 3000 );
							}
						}
						</script> 
					</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class='spacer-lg'></div>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
$joined = "joined";
}

if (isset($_GET['g']) && !isset($joined)){
		if (v::alnum()->length(32)->validate($_GET['g']) && $_GET['g'] != "notfound"){
			if (isset($_GET['l']) && v::alnum()->length(6,9)->validate($_GET['l'])){
				$_SESSION['link'] = $_GET['l'];
			}
			$group = $_GET['g'];
			
			$db->query("SELECT leader_name FROM `cust_groups` WHERE `group` = :g");
			$db->bind(":g",$group);
			if($result = $db->single()){
				?>
				<body>
					<div class="container-fluid">
						<div class="spacer-md hidden-xs"></div>
						<div class="row">
								<div id="main-cont" class="col-sm-8 col-sm-offset-2 col-xs-12 main-cont">
									<?php include "nav.php";  ?>
									<div class="spacer-md"></div>
									<div class="row" id="title_row">
										<div class="col-sm-12 text-center">
											<h3>Joining group queue currently run by <strong><?php echo $result['leader_name']; ?></strong></h3>
											<div class="spacer-sm"></div>
											<hr class="hr to-edge">
											<div class="spacer"></div>
										</div>
									</div>
									<div class="row messages_row" id="messages_row">
										<div class="col-sm-12">
											<div id="messages">
											</div>
										</div>
									</div>
									<div class="row" id="main_disp_row">
										<div class="col-xs-12 text-center" id="main_disp">
												<div class="form-group">
													<label for="playerName">Player Name (Please use your in game name!):</label>
													<input type="text" class="form-control" id="playerName" placeholder="PlayerName">
													<input type="hidden" id="groupN" value="<?php echo $group; ?>" />
												</div>
												<button id="join-queue" class="btn btn-default">Join Queue</button>
										</div>
									</div>
									<?php 
									modals("all");
									include "footer.php"; 
									?>
									<script type='text/javascript'>
									var http_root = '<?php echo $httproot ?>';
									$("#join-queue").click(function(e){
										if ($("#playerName").val() ==""){
											$("#modalSmallTitle").html("Whoops");
											$("#modalSmallBody").html("Please enter a player name!");
											$("#modal-small").modal("show");
										} else {
											$.ajax({
											 type: "POST",
											 url: 'custqhandler.php',
											 data: {"name":$("#playerName").val(),"group":$("#groupN").val(),"join":1},
											 success: function(data)
											 {
												 var response = $.parseJSON(data);
													if(response.state == "success"){
														location.reload();
													}
													if(response.state == "nameError"){
														$(".modal-title").html(response.mhead);
														$(".modal-body").html(response.mbody);
														$("#modal-small").modal("show");
													}
											 }
										 });
										}
									})
									
									
									</script> 
								</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class='spacer-lg'></div>
							</div>
						</div>
					</div>
				</body>
			</html>
				<?php
			} else {
				?>
				 <body>
					<div class="container-fluid">
						<div class="spacer-md hidden-xs"></div>
						<div class="row">
								<div id="main-cont" class="col-sm-8 col-sm-offset-2 col-xs-12 main-cont">
									<?php include "nav.php";  ?>
									<div class="spacer-md"></div>
									<div class="row" id="title_row">
										<div class="col-sm-12 text-center">
											<h1>Uh oh...</h1>
											<h3><strong>We can't seem to find the group you're trying to join! Please double check the link!</strong></h3>
											<div class="spacer-sm"></div>
										</div>
									</div>
									<?php 
									modals("all");
									include "footer.php"; 
									?> 
								</div>
						</div>
					</div>
				</body>
			</html>
				<?php
			}
		} else {
			?>
			<body>
					<div class="container-fluid">
						<div class="spacer-md hidden-xs"></div>
						<div class="row">
								<div id="main-cont" class="col-sm-8 col-sm-offset-2 col-xs-12 main-cont">
									<?php include "nav.php";  ?>
									<div class="spacer-md"></div>
									<div class="row" id="title_row">
										<div class="col-sm-12 text-center">
											<h1>Uh oh...</h1>
											<h3><strong>We can't seem to find the group you're trying to join! Please double check the link!</strong></h3>
											<div class="spacer-sm"></div>
										</div>
									</div>
									<?php 
									modals("all");
									include "footer.php"; 
									?> 
								</div>
						</div>
					</div>
				</body>
			</html>
			<?php
		}

}

if (!isset($_GET['g']) && !isset($_SESSION['cust']['group'])){
?>
<body>
    <div class="container-fluid">
			<div class="spacer-md hidden-xs"></div>
			<div class="row">
					<div id="main-cont" class="col-sm-8 col-sm-offset-2 col-xs-12 main-cont">
						<?php include "nav.php";  ?>
						<div class="spacer-md"></div>
						<div class="row" id="title_row">
							<div class="col-sm-12 text-center">
								<h1>OverwatchMM Custom Group Queue</h1>
								<h3><strong>This is currently a beta feature, and may have some bugs!</strong></h3>
								<h3><strong>Please use the <a href="<?php echo $httproot ?>/contact.php">contact</a> page to report any problems.</strong></h3>
								<div class="spacer-sm"></div>
								<hr class="hr to-edge">
								<div class="spacer"></div>
							</div>
						</div>
						<div class="row messages_row" id="messages_row">
							<div class="col-sm-12">
								<div id="messages">
								</div>
							</div>
						</div>
						<div class="row" id="main_disp_row">
							<div class="col-xs-12 text-center" id="main_disp">
									<div class="form-group">
										<label for="playerName">Player Name (Please use your in game name!):</label>
										<input type="text" class="form-control" id="playerName" placeholder="PlayerName">
									</div>
									<button id="start-queue" class="btn btn-default">Start Queue</button>
							</div>
						</div>
						<?php 
						modals("all");
						include "footer.php"; 
						?>
						<script type='text/javascript'>
						var http_root = '<?php echo $httproot ?>';
						$("#start-queue").click(function(e){
							if ($("#playerName").val() ==""){
								$("#modalSmallTitle").html("Whoops");
								$("#modalSmallBody").html("Please enter a player name!");
								$("#modal-small").modal("show");
							} else {
								$.ajax({
								 type: "POST",
								 url: 'custqhandler.php',
								 data: {"name":$("#playerName").val(),"create":1},
								 success: function(data)
								 {
									 var response = $.parseJSON(data);
									 console.log("data: "+data);
										if(response.state == "success"){
										location.reload();
									}
								 }
							 });
							}
						})
						
						
						</script> 
					</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class='spacer-lg'></div>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
}
?>