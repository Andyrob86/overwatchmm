<?php 
$pagetitle = "Overwatch Match Maker - Contact Us!";
$hideregion = true;
$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"/overwatchmm/www":"";
include_once "{$root}/../vendor/autoload.php";
set_debug();
if ( is_session_started() === FALSE ) {
	$session = new session();
	$session->start_session('_s', true);
}
include "header.php";
use Respect\Validation\Validator as v;
?>
<body>	
	<div class="container-fluid">
		<div class="spacer-md hidden-xs"></div>
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 col-xs-12  main-cont">
				<!-- Navigation -->
				<?php include 'nav.php'; ?>
				<div class="spacer-md"></div>
				<div class="row">
					<div class="col-sm-12 text-center">
						<p>Want to get in touch with me? Fill out the form below to send me a message and I will try to get back to you within 24 hours!</p>
							<div id="messages"></div>
							<form name="sentMessage" id="contactForm" class='contact-form' novalidate>
									<div class="row control-group">
											<div class="form-group col-xs-10 col-xs-offset-1  floating-label-form-group controls">
													<label>Name</label>
													<input type="text" class="form-control" placeholder="Name" id="name" required data-validation-required-message="Please enter your name.">
													<p class="help-block text-danger"></p>
											</div>
									</div>
									<div class="row control-group">
											<div class="form-group col-xs-10 col-xs-offset-1  floating-label-form-group controls">
													<label>Email Address</label>
													<input type="email" class="form-control" placeholder="Email Address" id="email" required data-validation-required-message="Please enter your email address.">
													<p class="help-block text-danger"></p>
											</div>
									</div>
									<div class="row control-group">
											<div class="form-group col-xs-10 col-xs-offset-1  floating-label-form-group controls">
													<label>Message</label>
													<textarea rows="5" class="form-control" placeholder="Message" id="message" required data-validation-required-message="Please enter a message."></textarea>
													<p class="help-block text-danger"></p>
											</div>
									</div>
									<br>
									<div id="success"></div>
									<div class="row">
											<div class="form-group col-xs-10 col-xs-offset-1 ">
													<button type="submit" class="btn btn-default">Send</button>
											</div>
									</div>
							</form>
					</div>
				</div>
				<?php include 'footer.php'; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class='spacer-lg'></div>
			</div>
		</div>
	</div>
</body>
</html>