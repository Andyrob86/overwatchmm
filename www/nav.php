    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<?php if(!isset($hideregion)){
									echo "<ul id='reg_ul' class='nav navbar-nav navbar-left reg-ul'>
									<li>
										<label class='reg-label' for='reg_sel'>Region:</label>
										<select class='reg-select' id='reg_sel'>
											<option value='us'>Americas</option>
											<option value='eu'>Europe</option>
											<option value='as'>Asia</option>
										</select> 
									</li>
								</ul>";
								} ?>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="/<?php echo ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"overwatchmm/www":"";?>">Home</a>
                    </li>
										<li>
                        <a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"/overwatchmm/www/groupq.php":"/groupq";?>">Group Manager</a>
                    </li>
                    <li>
                        <a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"/overwatchmm/www/about.php":"/about";?>">About</a>
                    </li>
                    <li>
                        <a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"/overwatchmm/www/contact.php":"/contact";?>">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>