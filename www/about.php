<?php 
$pagetitle = "Overwatch Match Maker - About";
$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"/overwatchmm/www":"";
include_once "{$root}/../vendor/autoload.php";
set_debug();
if ( is_session_started() === FALSE ) {
	$session = new session();
	$session->start_session('_s', true);
}
include "header.php";
$hideregion = true;
use Respect\Validation\Validator as v;
?>
<body>	
	<div class="container-fluid">
		<div class="spacer-md hidden-xs"></div>
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 col-xs-12  main-cont">
				<!-- Navigation -->
				<?php include 'nav.php'; ?>
				<div class="spacer-md"></div>
				<div class="row">
					<div class="col-sm-12 text-center">
						<h1>Go get your group on!</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 ">
						<p>Overwatchmm.com is a match making service for Overwatch. Unlike other match making services out there, Overwatchmm.com does the searching and group balancing for you. By selecting your primary and secondary roles, you can get into a balanced group (Or pick "Any" for a role, and fill in some empty spots!). Use this tool to find new friends, fill in a few empty spots in your current group (feature coming soon), or just get into a group for that bonus XP!!</p>
						<hr class="hr to-edge">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 ">
						<h2>Upcoming Features</h2>
						<ul class="list-group">
							<li class="list-group-item">News Page
								<ul><li>Keep up to date with changes and updates on Overwatchmm.com</li></ul>
							</li>
							<li class="list-group-item">Full Custom Game Lobby
								<ul><li>Soon, custom games will fill an entire lobby with players gathered from Overwatchmm.com. Each group will be balanced as needed, and group leader will manage groups in game.</li></ul>
							</li>
							<li class="list-group-item">Reserved Spots
								<ul><li>Already have part of a team? No problem! Overwatchmm.com will soon feature the ability to reserve some spaces prior to creating a group. Set your group member's roles, and Overwatchmm.com will fill in the blanks!</li></ul>
							</li>
							<li class="list-group-item">In Group Stats
								<ul><li>Enter your case sensetive player handle, and Overwatchmm.com will have player statistics available for all to see when you join a group.</li></ul>
							</li>
							<li class="list-group-item">Stand Alone Stats Page
								<ul><li>Interested in checking out your stats, or a the full stats of a player in your group? We will soon have a page dedicated to just that!</li></ul>
							</li>
							<li class="list-group-item">Whatever Else I Come Up With
								<ul><li>There will always be some improvements to be made (I can name a few already!), or new ideas that come up... like a player voting system... or a key command that sends a dancing banana to your teammates... the sky is the limit!</li></ul>
							</li>
						</ul>
					</div>
				</div>
				<?php include 'footer.php'; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class='spacer-lg'></div>
			</div>
		</div>
	</div>
</body>
</html>