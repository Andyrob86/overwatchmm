<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scrolable=no, maximum-scale=1.0, user-scalable=0'">
<meta name="description" content="An in depth matchmaking service for Overwatch - Get that sweet, sweet bonus XP!">
<meta name="keywords" content="OverwatchMM, Gaming, Overwatch, Match, Making, matchmaking">
<meta name="author" content="BleedTheWay">
<meta name="theme-color" content="#ffffff">
<title><?php echo isset($pagetitle)?$pagetitle : "Overwatch Match Maker"; ?></title>
<?php echo isset($addtoheader)?$addtoheader : ""; ?>
<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/manifest.json">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#46465b">
<meta name="msapplication-TileColor" content="#46465b">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<meta name="theme-color" content="#46465b">
<?php 
if ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false){
	if (isset($_GET['mintest']) && $_GET['mintest'] === "true"){
		echo "<link href='{$httproot}/css/2sbxxIsZln.min.css'  rel='stylesheet'>";
	} else {
		echo "<link href='/overwatchmm/www/css/bootstrap-minme.css' rel=stylesheet>";
		echo "<link href='/overwatchmm/www/css/clean-blog-minme.css' rel=stylesheet>";
		echo "<link href='/overwatchmm/www/css/quick.css' rel=stylesheet>";
	}
} else {
	echo "<link href='{$httproot}/css/2sbxxIsZln.min.css'  rel='stylesheet'>";
}
?>

<link href="https://fonts.googleapis.com/css?family=Black+Ops+One|Vesper+Libre" rel="stylesheet"> 
<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<style type='text/css'>
<?php 
echo isset($pagestyle)?$pagestyle : "";
$imgnum = rand(1,267);
switch($imgnum){
	case $imgnum <10:
		$imgnum = "00".$imgnum;
		break;
	case $imgnum >= 10 && $imgnum < 100:
		$imgnum = "0".$imgnum;
		break;
}
?>
@media only screen 
	and (max-device-width: 320px) { 
		body {
			background-image: url('<?php echo $httproot;?>/img/bgimg/xs/bg_img_<?php echo $imgnum; ?>.jpg');
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-size: cover;
		}
}

@media only screen 
	and (min-device-width: 321px)
  and (max-device-width: 667px)  { 
		body {
			background-image: url('<?php echo $httproot;?>/img/bgimg/mobile/bg_img_<?php echo $imgnum; ?>.jpg');
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-size: cover;
		}
}
@media only screen 
  and (min-device-width: 668px) {
		body {
			background-image: url('<?php echo $httproot;?>/img/bgimg/forweb/bg_img_<?php echo $imgnum; ?>.jpg');
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-size: cover;
		}
	}
</style>
</head>