<?php 
$pagetitle = "Overwatch Match Maker - admin";
$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"/overwatchmm/www":"";
include_once "{$root}/../vendor/autoload.php";
set_debug();
use Respect\Validation\Validator as v;
$db = new Database();

if(isset($_POST['email']) && v::email()->validate($_POST['email'])){
	$factory = new RandomLib\Factory;
	$generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::MEDIUM));
	$salt = $generator->generateString(64, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
	$key = hash('sha256', $_POST['email'].$salt);
	
	$db->query("INSERT INTO `access_list` (`email`,`key`,`salt`) VALUES (:e, :k, :s)");
	$db->bind(":e",$_POST['email']);
	$db->bind(":k",$key);
	$db->bind(":s",$salt);
	if($db->execute()){
		$rdata = array();
		$rdata['state'] = "success";
		$rdata['message'] = "<h1>User Added!</h1>";
		echo json_encode($rdata);
	}
	
}
if (isset($_POST['action']) && $_POST['action'] == "link"){
		include_once "../vendor/autoload.php";
		$factory = new RandomLib\Factory;
		$generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::MEDIUM));
		$token = $generator->generateString(32, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
		
		$db->query("INSERT INTO `temp_links` (`token`) VALUES (:token)");
		$db->bind(":token",$token);
		$db->execute();
		if (isset($_POST['phone']) && v::phone()->validate($_POST['phone'])){
			$text = "Here is your new temporary link to Overwatchmm.com: https://overwatchmm.com/?t=$token\n";
			$phone = str_replace("(","",str_replace(")","",str_replace("-","",$_POST['phone'])));
			$data = array(
			  'User'          => 'bleedtheway',
			  'Password'      => 'TimpfEZ2016!@#',
			  'PhoneNumbers'  => array($phone),
			  //'Groups'        => array(''),
			  // 'Subject'       => '',
			  'Message'       => $text,
			  // 'StampToSend'   => '1305582245',
			  'MessageTypeID' => 1
			  );
			 
			$curl = curl_init('https://app.eztexting.com/sending/messages?format=xml');
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($curl);
			curl_close($curl);
			$rdata = array();
			$rdata['state'] = "success";
			$rdata['message'] = "<h1>User Added!</h1>";
			echo json_encode($rdata);
		}else{
			$to = 'bleedtheway@overwatchmm.com'; // Add your email address inbetween the '' replacing yourname@yourdomain.com - This is where the form will send a message to.
			$email_subject = "Overwatchmm.com Temporary Link";
			$email_body = "Here is your new temporary link to Overwatchmm: https://overwatchmm.com/?t=$token\n";
			$headers = "From: noreply@overwatchmm.com\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
			$headers .= "Reply-To: noreply@overwatchmm.com\n";	
			mail($to,$email_subject,$email_body,$headers);	
		}
}
include "{$root}/header.php";
?>
<body>
	<div class="container-fluid">
		<div class="spacer-md"></div>
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 col-xs-12  main-cont">
			<div class="row messages_row" id="messages_row">
				<div class="col-sm-12">
					<div id="messages"></div>
				</div>
			</div>
			<?php 
			send_link_form();
			add_user_form();
			include "{$root}/footer.php"; 
			?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class='spacer-lg'></div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#add").submit(function(e){
				e.preventDefault();
				$.ajax({
					 type: "POST",
					 url: 'index.php',
					 data: {"email":$("#email").val()},
					 success: function(data)
					 {
						 console.log("data: " + data);
						 var response = $.parseJSON(data);
						 var info = undefined;
						if(response.state == "success"){
							$("#messages").html(response.message);
						}
					 }
				 });
			})
			$("#link").submit(function(e){
				e.preventDefault();
				$.ajax({
					 type: "POST",
					 url: 'index.php',
					 data: {"phone":$("#phone").val(),"action":"link"},
					 success: function(data)
					 {
						 console.log("data: " + data);
						 var response = $.parseJSON(data);
						 var info = undefined;
						if(response.state == "success"){
							$("#messages").html(response.message);
						}
					 }
				 });
			})
		})
	</script>
</body>
</html>