<?php

$root = $_SERVER['DOCUMENT_ROOT']."/overwatchmm/";

include "{$root}header.php";
use Respect\Validation\Validator as v;
if(isset($_POST['email']) && v::email()->validate($_POST['email'])){
	$factory = new RandomLib\Factory;
	$generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::MEDIUM));
	$salt = $generator->generateString(64, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
	$key = hash('sha256', $_POST['email'].$salt);
	$db = new Database();
	$db->query("INSERT INTO `access_list` (`email`,`key`,`salt`) VALUES (:e, :k, :s");
	$db->bind(":e",$_POST['email']);
	$db->bind(":k",$key);
	$db->bind(":s",$salt);
	if($db->execute()){
		$rdata = array();
		$rdata['state'] = "success";
		$rdata['message'] = "<h1>User Added!</h1>";
		echo json_encode($rdata);
	}
	
}
?>