<?php
use \Colors\RandomColor;
function create_group($inf){
	$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
	$rdata = array();
	$sdata = array();
	$time = time();
	$factory = new RandomLib\Factory;
	$generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::MEDIUM));
	$leaderToken = $generator->generateString(64, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
	$groupToken = $generator->generateString(64, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
	if ($inf['pr'] == "any"){
		$q = "INSERT INTO `groups_{$inf['reg']}_{$inf['plat']}` (`p1`, `p1r1`, `p1r2`, `group_token`, `leader_token`, `p1t`, `game_type`, `p1m`, `mic_req`, `".$inf['pr']."`, `last_dropped`, `last_active`) VALUES (:gtag, :r1, :r2, :gtoken, :ltoken, :p1t, :gtype, :pm, :mr, 1, $time, $time)";
	} else {
		$q = "INSERT INTO `groups_{$inf['reg']}_{$inf['plat']}` (`p1`, `p1r1`, `p1r2`, `group_token`, `leader_token`, `p1t`, `game_type`, `p1m`, `mic_req`, `need_".$inf['pr']."`, `".$inf['pr']."`, `".$inf['sr']."`, `last_dropped`, `last_active`) VALUES (:gtag, :r1, :r2, :gtoken, :ltoken, :p1t, :gtype, :pm, :mr, 0, 1, 1, $time, $time)";
	}
	$db = new Database();
	$db->query($q);
	$json = array("game" => array("chat" => array()), "players" => array("p1" => array("gt" => $inf['gt'], "pr" => $inf['pr'], "sr" => $inf['sr'], "m" => ($inf['have_mic'] == 0 ? "No" : "Yes")), "p2" => array("gt" => "", "pr" => "", "sr" => "", "m" => ""), "p3" => array("gt" => "", "pr" => "", "sr" => "", "m" => ""), "p4" => array("gt" => "", "pr" => "", "sr" => "", "m" => ""), "p5" => array("gt" => "", "pr" => "", "sr" => "", "m" => ""), "p6" => array("gt" => "", "pr" => "", "sr" => "", "m" => "")));
	$db->bind(':gtag',$inf['gt']);
	$db->bind(':r1',$inf['pr']);
	$db->bind(':r2',$inf['sr']);
	$db->bind(':mr',$inf['mic_req']);
	$db->bind(':pm',$inf['have_mic']);
	$db->bind(':gtype',$inf['type']);
	$db->bind(':p1t',$inf['pt']);
	$db->bind(':gtoken',$groupToken);
	$db->bind(':ltoken',$leaderToken);
	sleep(1);
	if ($db->execute()){
		$fp = fopen("{$root}/groups/$groupToken.json", 'a');
		flock($fp, LOCK_EX);
		usleep(rand(20,80));
		fwrite($fp, json_encode($json));
		flock($fp, LOCK_UN);
		fclose($fp);
		$sdata['lt'] = $leaderToken;
		$sdata['gt'] = $inf['gt'];
		$sdata['plat'] = $inf['plat'];
		$sdata['pt'] = $inf['pt'];
		$sdata['pos'] = "p1";
		$sdata['reg'] = $inf['reg'];
		$sdata['group'] = $groupToken; 
		$sdata['color'] = RandomColor::one(array('format'=>'hex'));
		$_SESSION['data'] = $sdata;
		$rdata['state'] = "leader";
		echo json_encode($rdata);
	} else {
		$rdata['message'] = "<div class='alert alert-danger fade in out'><a href='#' class='close' data-dismiss='alert' aria-label='close'><i class='fa fa-times-circle fa-lg fa-fw' aria-hidden='true'></i></a>Something went wrong trying to create your group!.<div>";
		echo json_encode($rdata);
	}
	
};

function find_empty_group($inf){
	$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];	$sdata = array();
	foreach($inf['queries'] as $key => $query){
		if(!isset($joined)){
			$db = new Database();
			$db->query($query);
			if($results = $db->single()){
				if (!strpos($results['kicked'], $inf['pt']) !== false) {
					$groupId = $results['id'];
					$groupToken = $results['group_token'];
					$q = "SELECT p1, p2, p3, p4, p5, p6 FROM `groups_{$inf['reg']}_{$inf['plat']}` WHERE `id` = :gid";
					$db->query($q);
					$db->bind(":gid",$groupId);
					if ($results = $db->single()){
						if ($results['p2'] == null){
							$q = "UPDATE `groups_{$inf['reg']}_{$inf['plat']}` SET `p2` = :p, `p2t` = :pt, `p2r1` = :r1, `p2r2` = :r2, `p2m` = :pm,`open_count` = `open_count` - 1, `$inf[pr]` = $inf[pr] + 1, `$inf[sr]` = $inf[sr] + 1  ".($inf['pr'] == "any" ? "" : ", `need_".$inf['pr']."` = 0 ")."WHERE `id` = :gid";
							$db->query($q);
							$db->bind(":p",$inf['gt']);
							$db->bind(":pt",$inf['pt']);
							$db->bind(":r1",$inf['pr']);
							$db->bind(":r2",$inf['sr']);
							$db->bind(":pm",$inf['have_mic']);
							$db->bind(":gid",$groupId);
							if ($db->execute()){
								$sdata['pos'] = "p2";
								$joined = true;
								add_p_to_json($inf, 'p2', $groupToken);
							}
							continue;
						};
						if ($results['p3'] == null){
							$q = "UPDATE `groups_{$inf['reg']}_{$inf['plat']}` SET `p3` = :p, `p3t` = :pt, `p3r1` = :r1, `p3r2` = :r2, `p3m` = :pm ,`open_count` = `open_count` - 1, `$inf[pr]` = $inf[pr] + 1, `$inf[sr]` = $inf[sr] + 1  ".($inf['pr'] == "any" ? "" : ", `need_".$inf['pr']."` = 0 ")."WHERE `id` = :gid";
							$db->query($q);
							$db->bind(":p",$inf['gt']);
							$db->bind(":pt",$inf['pt']);
							$db->bind(":r1",$inf['pr']);
							$db->bind(":r2",$inf['sr']);
							$db->bind(":pm",$inf['have_mic']);
							$db->bind(":gid",$groupId);
							if ($db->execute()){
								$sdata['pos'] = "p3";
								$joined = true;
								add_p_to_json($inf, 'p3', $groupToken);
							}
							continue;
						};
						if ($results['p4'] == null){
							$q = "UPDATE `groups_{$inf['reg']}_{$inf['plat']}` SET `p4` = :p, `p4t` = :pt, `p4r1` = :r1, `p4r2` = :r2, `p4m` = :pm ,`open_count` = `open_count` - 1, `$inf[pr]` = $inf[pr] + 1, `$inf[sr]` = $inf[sr] + 1 ".($inf['pr'] == "any" ? "" : ", `need_".$inf['pr']."` = 0 ")."WHERE `id` = :gid";
							$db->query($q);
							$db->bind(":p",$inf['gt']);
							$db->bind(":pt",$inf['pt']);
							$db->bind(":r1",$inf['pr']);
							$db->bind(":r2",$inf['sr']);
							$db->bind(":pm",$inf['have_mic']);
							$db->bind(":gid",$groupId);
							if ($db->execute()){
								$sdata['pos'] = "p4";
								$joined = true;
								add_p_to_json($inf, 'p4', $groupToken);
							}
							continue;
						};
						if ($results['p5'] == null){
							$q = "UPDATE `groups_{$inf['reg']}_{$inf['plat']}` SET `p5` = :p, `p5t` = :pt, `p5r1` = :r1, `p5r2` = :r2, `p5m` = :pm ,`open_count` = `open_count` - 1, `$inf[pr]` = $inf[pr] + 1, `$inf[sr]` = $inf[sr] + 1  ".($inf['pr'] == "any" ? "" : ", `need_".$inf['pr']."` = 0 ")."WHERE `id` = :gid";
							$db->query($q);
							$db->bind(":p",$inf['gt']);
							$db->bind(":pt",$inf['pt']);
							$db->bind(":r1",$inf['pr']);
							$db->bind(":r2",$inf['sr']);
							$db->bind(":pm",$inf['have_mic']);
							$db->bind(":gid",$groupId);
							if ($db->execute()){
								$sdata['pos'] = "p5";
								$joined = true;
								add_p_to_json($inf, 'p5', $groupToken);
							}
							continue;
						};
						if ($results['p6'] == null){
							$q = "UPDATE `groups_{$inf['reg']}_{$inf['plat']}` SET `p6` = :p, `p6t` = :pt, `p6r1` = :r1, `p6r2` = :r2, `p6m` = :pm ,`open_count` = `open_count` - 1, `$inf[pr]` = $inf[pr] + 1, `$inf[sr]` = $inf[sr] + 1  ".($inf['pr'] == "any" ? "" : ", `need_".$inf['pr']."` = 0 ")."WHERE `id` = :gid";
							$db->query($q);
							$db->bind(":p",$inf['gt']);
							$db->bind(":pt",$inf['pt']);
							$db->bind(":r1",$inf['pr']);
							$db->bind(":r2",$inf['sr']);
							$db->bind(":pm",$inf['have_mic']);
							$db->bind(":gid",$groupId);
							if ($db->execute()){
								$sdata['pos'] = "p6";
								$joined = true;
								add_p_to_json($inf, 'p6', $groupToken);
							}
							continue;
						};
					}
				}
			}
		}
	}
	if (!isset($joined)){
		if ($inf['lead'] == 0){
			$rdata = array();
			$rdata['message'] = "<div align='center'>
				<h2>No open groups match your criteria!</h2>
				<p>Would you like to be a group leader and create a new group? It should help improve queue times!</p>
				<button>Create new group</button><button style='margin-left:15px;'>Search again</button></div>";
			$rdata['state'] = 'error';
			echo json_encode($rdata);
		};
		if ($inf['lead'] == 1 && !isset($joined)) {
			
			create_group($inf);
		};
	} else {
		$sdata['group'] = $groupToken;
		$sdata['pt'] = $inf['pt'];
		$sdata['gt'] = $inf['gt'];
		$sdata['plat'] = $inf['plat'];
		$sdata['reg'] = $inf['reg'];
		$sdata['color'] = RandomColor::one(array('format'=>'hex'));
		$_SESSION['data'] = $sdata;
		$rdata['message'] = "Group found! If page does not reload <a onclick='location.reload()'>click here</a>.";
		$rdata['state'] = "success";
		echo json_encode($rdata);
	}
	
}

function leave_group($c){
	$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];	$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"c:/xampp/htdocs/overwatchmm/www":"";
	$time = time();
	$rdata = array();
	$db = new Database();
	$q = "SELECT * FROM `groups_{$c['reg']}_{$c['plat']}` WHERE `group_token` = :group AND `{$c['pos']}` = :gt AND `{$c['pos']}t` = :pt";
	$db->query($q);
	$db->bind(":group",$c['group']);
	$db->bind(":gt",$c['gt']);
	$db->bind(":pt",$c['pt']);
	if($result = $db->single()){
		$id = $result['id'];
		if(isset($c['lt'])){
			$found = "";
			if($result['leader_token'] == $c['lt']){
				$inf = array();
				$factory = new RandomLib\Factory;
				$generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::MEDIUM));
				$newLeaderToken = $generator->generateString(64, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
				if ($result['p2'] != null && $found != "found"){
					$q = "UPDATE `groups_{$c['reg']}_{$c['plat']}` SET `p2` = null, `p2r1` = null, `p2r2` = null, `p2m` = 0, `p2t` = null, `open_count` = `open_count` + 1, `last_dropped` = $time,  `p1` = :newlead, `p1r1` = :newr1, `p1r2` = :newr2, `p1m` = :newm, `p1t` = :newp1t, `leader_token` = :newlt  WHERE `id` = :id";
					$db->query($q);
					$db->bind(":newlead",$result['p2']);
					$db->bind(":newr1",$result['p2r1']);
					$db->bind(":newr2",$result['p2r2']);
					$db->bind(":newm",$result['p2m']);
					$db->bind(":newp1t",$result['p2t']);
					$db->bind(":newlt",$newLeaderToken);
					$oldPos = "p2";
					$pt = $result['p2t'];
					$inf['gt'] = $result['p2'];
					$inf['pr'] = $result['p2r1'];
					$inf['sr'] = $result['p2r2'];
					$inf['have_mic'] = $result['p2m'];
					$found = "found";
				}
				if ($result['p3'] != null && $found != "found"){
					$q = "UPDATE `groups_{$c['reg']}_{$c['plat']}` SET `p3` = null, `p3r1` = null, `p3r2` = null, `p3m` = 0, `p3t` = null, `open_count` = `open_count` + 1, `last_dropped` = $time,  `p1` = :newlead, `p1r1` = :newr1, `p1r2` = :newr2, `p1m` = :newm, `p1t` = :newp1t, `leader_token` = :newlt  WHERE `id` = :id";
					$db->query($q);
					$db->bind(":newlead",$result['p3']);
					$db->bind(":newr1",$result['p3r1']);
					$db->bind(":newr2",$result['p3r2']);
					$db->bind(":newm",$result['p3m']);
					$db->bind(":newp1t",$result['p3t']);
					$db->bind(":newlt",$newLeaderToken);
					$oldPos = "p3";
					$pt = $result['p3t'];
					$inf['gt'] = $result['p3'];
					$inf['pr'] = $result['p3r1'];
					$inf['sr'] = $result['p3r2'];
					$inf['have_mic'] = $result['p3m'];
					$found = "found";
				}
				if ($result['p4'] != null && $found != "found"){
					$q = "UPDATE `groups_{$c['reg']}_{$c['plat']}` SET `p4` = null, `p4r1` = null, `p4r2` = null, `p4m` = 0, `p4t` = null, `open_count` = `open_count` + 1, `last_dropped` = $time,  `p1` = :newlead, `p1r1` = :newr1, `p1r2` = :newr2, `p1m` = :newm, `p1t` = :newp1t, `leader_token` = :newlt  WHERE `id` = :id";
					$db->query($q);
					$db->bind(":newlead",$result['p4']);
					$db->bind(":newr1",$result['p4r1']);
					$db->bind(":newr2",$result['p4r2']);
					$db->bind(":newm",$result['p4m']);
					$db->bind(":newp1t",$result['p4t']);
					$db->bind(":newlt",$newLeaderToken);
					$oldPos = "p4";
					$pt = $result['p4t'];
					$inf['gt'] = $result['p4'];
					$inf['pr'] = $result['p4r1'];
					$inf['sr'] = $result['p4r2'];
					$inf['have_mic'] = $result['p4m'];
					$found = "found";
				}
				if ($result['p5'] != null && $found != "found"){
					$q = "UPDATE `groups_{$c['reg']}_{$c['plat']}` SET `p5` = null, `p5r1` = null, `p5r2` = null, `p5m` = 0, `p5t` = null, `open_count` = `open_count` + 1, `last_dropped` = $time,  `p1` = :newlead, `p1r1` = :newr1, `p1r2` = :newr2, `p1m` = :newm, `p1t` = :newp1t, `leader_token` = :newlt  WHERE `id` = :id";
					$db->query($q);
					$db->bind(":newlead",$result['p5']);
					$db->bind(":newr1",$result['p5r1']);
					$db->bind(":newr2",$result['p5r2']);
					$db->bind(":newm",$result['p5m']);
					$db->bind(":newp1t",$result['p5t']);
					$db->bind(":newlt",$newLeaderToken);
					$oldPos = "p5";
					$pt = $result['p5t'];
					$inf['gt'] = $result['p5'];
					$inf['pr'] = $result['p5r1'];
					$inf['sr'] = $result['p5r2'];
					$inf['have_mic'] = $result['p5m'];
					$found = "found";
				}
				if ($result['p6'] != null && $found != "found"){
					$q = "UPDATE `groups_{$c['reg']}_{$c['plat']}` SET `p6` = null, `p6r1` = null, `p6r2` = null, `p6m` = 0, `p6t` = null, `open_count` = `open_count` + 1, `last_dropped` = $time,  `p1` = :newlead, `p1r1` = :newr1, `p1r2` = :newr2, `p1m` = :newm, `p1t` = :newp1t, `leader_token` = :newlt  WHERE `id` = :id";
					$db->query($q);
					$db->bind(":newlead",$result['p6']);
					$db->bind(":newr1",$result['p6r1']);
					$db->bind(":newr2",$result['p6r2']);
					$db->bind(":newm",$result['p6m']);
					$db->bind(":newp1t",$result['p6t']);
					$db->bind(":newlt",$newLeaderToken);
					$oldPos = "p6";
					$pt = $result['p6t'];
					$inf['gt'] = $result['p6'];
					$inf['pr'] = $result['p6r1'];
					$inf['sr'] = $result['p6r2'];
					$inf['have_mic'] = $result['p6m'];
					$found = "found";
				}

				if($result['open_count'] == 5){
					$q = "DELETE FROM `groups_{$c['reg']}_{$c['plat']}` WHERE `id` = :id";
					$oldPos = "p1";
					$pt = $result['p1t'];
					$db->query($q);
					$found = "found";
					$del = true;
				}
				$db->bind(":id",$id);
				if($db->execute()){
					if($result['open_count'] != 5){
						$db->query("INSERT INTO `new_lead` (`player_token`,`new_leader_token`,`group`) VALUES (:pt, :nlt, :g)");
						$db->bind(":pt",$pt);
						$db->bind(":nlt",$newLeaderToken);
						$db->bind(":g",$c['group']);
						$db->execute();
					}
					$_SESSION['data']['group'] = null;
					if(isset($del)){
						unlink("{$root}/groups/".$c['group'].".json");
					} else {
						leader_leave_json($pt, $inf, $c['pos'], $oldPos, $c['group']);
					}
				}
				if ($found == "found"){
					$rdata['message'] = "<p>You have left your group. If your page does not refresh, <a href='#' onClick='window.location.reload();return false;'>Click Here.</a></p>";
					$rdata['state'] = "success";
					echo json_encode($rdata);
				} else {
					$rdata['message'] = "<div class='alert alert-danger fade in out'><a href='#' class='close' data-dismiss='alert' aria-label='close'><i class='fa fa-times-circle fa-lg fa-fw' aria-hidden='true'></i></a>Something went wrong trying to leave your group!.<div>";
					$rdata['state'] = "error";
					echo json_encode($rdata);
				}
			}
		}
		if(!isset($c['lt'])){
			if($result['open_count'] == 5){
				$q = "DELETE FROM `groups_{$c['reg']}_{$c['plat']}` WHERE `id` = :id";
				$del = true;
			} else {
			$q = "UPDATE `groups_{$c['reg']}_{$c['plat']}` SET `{$c['pos']}` = null, `{$c['pos']}r1` = null, `{$c['pos']}r2` = null, `{$c['pos']}m` = 0, `{$c['pos']}t` = null, `open_count` = `open_count` + 1, `last_dropped` = $time WHERE `id` = :id";
			}
			$db->query($q);
			$db->bind(":id",$id);
			if($db->execute()){
				$_SESSION['data']['group'] = null;
			
				if(isset($del)){
					unlink("{$root}/groups/".$c['group'].".json");
				} else {
					remove_p_from_json($c['pos'], $c['group']);
				}
				$rdata['message'] = "<p>You have left your group. If your page does not refresh, <a href='#' onClick='window.location.reload();return false;'>Click Here.</a></p>";
				$rdata['state'] = "success";
				echo json_encode($rdata);
			} else {
				$rdata['message'] = "<div class='alert alert-danger fade in out'><a href='#' class='close' data-dismiss='alert' aria-label='close'><i class='fa fa-times-circle fa-lg fa-fw' aria-hidden='true'></i></a>Something went wrong!.<div>";
				$rdata['state'] = "error";
				echo json_encode($rdata);
			}
		}
	} else {
		$rdata['message'] = "<div class='alert alert-danger fade in out'><a href='#' class='close' data-dismiss='alert' aria-label='close'><i class='fa fa-times-circle fa-lg fa-fw' aria-hidden='true'></i></a>Something went wrong!.<div>";
		$rdata['state'] = "error";
		echo json_encode($rdata);
	}
	
}

?>