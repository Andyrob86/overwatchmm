<?php
function create_player_table(){
	if(check_for_lead()){
		echo "<h2 class='text-center'>You are the group leader!</h2>
					<strong>Invite your team members to the game as they join!</strong>
					<div class='spacer-sm'></div>
					<div class='table-responsive'>
						<table class='table table-hover'>
							<thead>
								<tr>
									<td>Player</td>
									<td>Primary</td>
									<td>Secondary</td>
									<td>Mic?</td>
									<td style='width:15%'>Controls</td>
								</tr>
							</thead>
							<tbody>
								<tr id='p1_tr' style='display:none'><td id='p1_gt_td'></td><td id='p1_pr_td'></td><td id='p1_sr_td'></td><td id='p1_m_td'></td><td id='p1_lc_td'><button class='btn btn-xs btn-danger' id='leave_group' >Leave Group</button></form></td></tr>
								<tr id='p2_tr' style='display:none'><td id='p2_gt_td'></td><td id='p2_pr_td'></td><td id='p2_sr_td'></td><td id='p2_m_td'></td><td id='p2_lc_td'><button class='btn btn-xs btn-success promote' id='promote_p2' player='' value='p2' ><i class='fa fa-angle-double-up'></i></button><button value='p2' class='btn btn-xs btn-warning kick' id='kick_p2' player='' ><i class='fa fa-ban'></i></button></td></tr>
								<tr id='p3_tr' style='display:none'><td id='p3_gt_td'></td><td id='p3_pr_td'></td><td id='p3_sr_td'></td><td id='p3_m_td'></td><td id='p3_lc_td'><button class='btn btn-xs btn-success promote' id='promote_p3' player='' value='p3' ><i class='fa fa-angle-double-up'></i></button><button value='p3' class='btn btn-xs btn-warning kick' id='kick_p3' player='' ><i class='fa fa-ban'></i></button></td></tr>
								<tr id='p4_tr' style='display:none'><td id='p4_gt_td'></td><td id='p4_pr_td'></td><td id='p4_sr_td'></td><td id='p4_m_td'></td><td id='p4_lc_td'><button class='btn btn-xs btn-success promote' id='promote_p4' player='' value='p4' ><i class='fa fa-angle-double-up'></i></button><button value='p4' class='btn btn-xs btn-warning kick' id='kick_p4' player='' ><i class='fa fa-ban'></i></button></td></tr>
								<tr id='p5_tr' style='display:none'><td id='p5_gt_td'></td><td id='p5_pr_td'></td><td id='p5_sr_td'></td><td id='p5_m_td'></td><td id='p5_lc_td'><button class='btn btn-xs btn-success promote' id='promote_p5' player='' value='p5' ><i class='fa fa-angle-double-up'></i></button><button value='p5' class='btn btn-xs btn-warning kick' id='kick_p5' player='' ><i class='fa fa-ban'></i></button></td></tr>
								<tr id='p6_tr' style='display:none'><td id='p6_gt_td'></td><td id='p6_pr_td'></td><td id='p6_sr_td'></td><td id='p6_m_td'></td><td id='p6_lc_td'><button class='btn btn-xs btn-success promote' id='promote_p6' player='' value='p6' ><i class='fa fa-angle-double-up'></i></button><button value='p6' class='btn btn-xs btn-warning kick' id='kick_p6' player='' ><i class='fa fa-ban'></i></button></td></tr>
							</tbody>
						</table>
					</div>";
	} else {
		echo "
					<div class='table-responsive' id='player_table' style='display:none'>
						<table class='table table-hover'>
							<thead>
								<tr>
									<td>Player</td>
									<td>Primary</td>
									<td>Secondary</td>
									<td>Mic?</td>
									<td>Controls</td>
								</tr>
							</thead>
							<tbody>
								<tr id='p1_tr' style='display:none'><td id='p1_gt_td'></td><td id='p1_pr_td'></td><td id='p1_sr_td'></td><td id='p1_m_td'></td><td id='p1_pc_td'></td></tr>
								<tr id='p2_tr' style='display:none'><td id='p2_gt_td'></td><td id='p2_pr_td'></td><td id='p2_sr_td'></td><td id='p2_m_td'></td><td id='p2_pc_td'></td></tr>
								<tr id='p3_tr' style='display:none'><td id='p3_gt_td'></td><td id='p3_pr_td'></td><td id='p3_sr_td'></td><td id='p3_m_td'></td><td id='p3_pc_td'></td></tr>
								<tr id='p4_tr' style='display:none'><td id='p4_gt_td'></td><td id='p4_pr_td'></td><td id='p4_sr_td'></td><td id='p4_m_td'></td><td id='p4_pc_td'></td></tr>
								<tr id='p5_tr' style='display:none'><td id='p5_gt_td'></td><td id='p5_pr_td'></td><td id='p5_sr_td'></td><td id='p5_m_td'></td><td id='p5_pc_td'></td></tr>
								<tr id='p6_tr' style='display:none'><td id='p6_gt_td'></td><td id='p6_pr_td'></td><td id='p6_sr_td'></td><td id='p6_m_td'></td><td id='p6_pc_td'></td></tr>
							</tbody>
						</table>
					</div>";
	}
}

function create_cust_table(){
		?>
		<div class='spacer-sm'></div>
		<ul id='players_list' class='list-group'></ul>

	<?php
}

function offer_lead($switch = 0){
	switch($switch){
		case 0:
			$msg =
			"
			<button>Create new group</button>
			";
			break;
		case 1:
			$msg =
			"
			<div align='center'>
				<h2>No open groups found!</h2>
				<p>Would you like to be a group leader and create a new group? It should help improve queue times!</p>
				<button>Create new group</button><button style='margin-left:15px;'>Search again</button>
			</div>
			";
			break;
		case 2:
			$msg =
				"
				<div align='center'>
					<h2>No open groups found!</h2>
					<p>Get the ball rolling by stepping up to the plate, and being a group leader!</p>
				</div>
				";
			break;
	}	
	return $msg;
}

function join_form() {
	$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"/overwatchmm/www":"";
	echo
	"
	<div id='plat_row'>
		<div class='row'>
			<div class='col-xs-4 '  align='center'>
			<div class='spacer hidden-xs'></div>
				<img style='cursor:pointer' class='img-responsive ' id='plat_pc' src='{$httproot}/img/assets/pc.png'>
				<div class='spacer'></div>
			</div>
			<div class='col-xs-4'  align='center'>
				<div class='spacer hidden-xs'></div>
				<img style='cursor:pointer' class='img-responsive ' id='plat_xbox' src='{$httproot}/img/assets/xbox.png'>
				<div class='spacer'></div>
			</div>
			<div class='col-xs-4'  align='center'>
				<div class='spacer hidden-xs'></div>
				<img style='cursor:pointer' class='img-responsive ' id='plat_ps4' src='{$httproot}/img/assets/ps4.png'>
				<div class='spacer hidden-xs'></div>
			</div>
		</div>
	</div>
	<div id='form_row' style='display:none'>
		<div class='row'>
			<div class='col-sm-1 col-sm-offset-1'>
				<p><span style='float:left;cursor:pointer;font-size:16px' id=form_back>back</span></p>
			</div>
		</div>
		<div class='row'>
			<div class='col-sm-12' id='pc_form_div' style='display:none'>
				<h2 align='center'>Join Group on PC</h2>
				<form class='form-horizontal' role='form' id='joinpc' method='POST' action=''>
					<div class='form-group'>
						<label class='control-label col-sm-2' for='gamer_tag'>Battle.net Name:</label>
						<div class='col-sm-10'>
							<input type='text' class='form-control' id='gamer_tag' name='gamer_tag' placeholder='yourname#1234'>
						</div>
					</div>
					<div class='form-group'>
						<label class='control-label col-sm-2 lead-info' for='lead'>Will you lead <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-10'>
							<label class='radio-img'>
								<input type='radio' checked name='lead' value='1' />
								<img src='{$httproot}/img/assets/icon/yes.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' name='lead' value='0' />
								<img src='{$httproot}/img/assets/icon/no.png'>
							</label>
						</div>
					</div>
					<div class='form-group'>
						<label class='control-label col-sm-2 game-type-info' for='game_type'>Game type <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-10'>
							<label class='radio-img'>
								<input type='radio' checked name='game_type' value='quick' />
								<img src='{$httproot}/img/assets/icon/quick.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' name='game_type' value='competitive' />
								<img src='{$httproot}/img/assets/icon/competitive.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' name='game_type' value='custom' />
								<img src='{$httproot}/img/assets/icon/custom.png'>
							</label>
						</div>
					</div>
					<div class='form-group' id='pr_pc'>
						<label class='control-label col-sm-2 role-info' for='pr'>Primary role <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-10'>
							<label class='radio-img'>
								<input type='radio' id='pr_pc_any' checked name='pr' value='any' />
								<img id='pr_pc_any_img' src='{$httproot}/img/assets/icon/any.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='pr_pc_off' name='pr' value='offense' />
								<img id='pr_pc_off_img' src='{$httproot}/img/assets/icon/offense.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='pr_pc_def' name='pr' value='defense' />
								<img id='pr_pc_def_img' src='{$httproot}/img/assets/icon/defense.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='pr_pc_tank' name='pr' value='tank' />
								<img id='pr_pc_tank_img' src='{$httproot}/img/assets/icon/tank.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='pr_pc_supp' name='pr' value='support' />
								<img id='pr_pc_supp_img' src='{$httproot}/img/assets/icon/support.png'>
							</label>
						</div>
					</div>
					<div class='form-group' id='sr_pc' style='display:none'>
						<label class='control-label col-sm-2 role-info' for='sr'>Secondary role <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-10'>
							<label class='radio-img'>
								<input type='radio' id='sr_pc_any' checked name='sr' value='any' />
								<img src='{$httproot}/img/assets/icon/any.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='sr_pc_off' name='sr' value='offense' />
								<img src='{$httproot}/img/assets/icon/offense.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='sr_pc_def' name='sr' value='defense' />
								<img src='{$httproot}/img/assets/icon/defense.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='sr_pc_tank' name='sr' value='tank' />
								<img src='{$httproot}/img/assets/icon/tank.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='sr' name='sr' value='support' />
								<img src='{$httproot}/img/assets/icon/support.png'>
							</label>
						</div>
					</div>
					<div class='form-group'>
						<label class='control-label col-sm-2 mic-info' for='have_mic'>Have Mic <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-4'>
							<label class='radio-img'>
								<input type='radio' name='have_mic' value='1' />
								<img src='{$httproot}/img/assets/icon/mic-yes.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' checked name='have_mic' value='0' />
								<img src='{$httproot}/img/assets/icon/mic-no.png'>
							</label>
						</div>
						<label class='control-label col-sm-2 mic-req-info' for='mic_req'>Require Mic <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-4'>
							<label class='radio-img'>
								<input type='radio' name='mic_req' value='1' />
								<img src='{$httproot}/img/assets/icon/yes.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' checked name='mic_req' value='0' />
								<img src='{$httproot}/img/assets/icon/no.png'>
							</label>
						</div>
					</div>
					<input type='hidden' name='platform' value='pc' />
					<div class='form-group'>
						<div class='col-sm-12 ' align='center'>
							<button type='submit' class='btn btn-xs btn-default' >Submit</button>
						</div>
					</div>
				</form>
			</div>
			<div class='col-sm-12' id='xbox_form_div' style='display:none'>
				<h2 align='center'>Join Group on Xbox One</h2>
				<form class='form-horizontal' role='form' id='joinxbox' method='POST' action=''>
					<div class='form-group'>
						<label class='control-label col-sm-2' for='gamer_tag'>Xbox Gamertag:</label>
						<div class='col-sm-10'>
							<input type='text' class='form-control' id='gamer_tag' name='gamer_tag' placeholder='YourGamertag'>
						</div>
					</div>
					<div class='form-group'>
						<label class='control-label col-sm-2 lead-info' for='lead'>Will you lead <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-10'>
							<label class='radio-img'>
								<input type='radio' checked name='lead' value='1' />
								<img src='{$httproot}/img/assets/icon/yes.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' name='lead' value='0' />
								<img src='{$httproot}/img/assets/icon/no.png'>
							</label>
						</div>
					</div>
					<div class='form-group'>
						<label class='control-label col-sm-2 game-type-info' for='game_type'>Game type <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-10'>
							<label class='radio-img'>
								<input type='radio' checked name='game_type' value='quick' />
								<img src='{$httproot}/img/assets/icon/quick.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' name='game_type' value='competitive' />
								<img src='{$httproot}/img/assets/icon/competitive.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' name='game_type' value='custom' />
								<img src='{$httproot}/img/assets/icon/custom.png'>
							</label>
						</div>
					</div>
					<div class='form-group' id='pr_xbox'>
						<label class='control-label col-sm-2 role-info' for='pr'>Primary role <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-10'>
							<label class='radio-img'>
								<input type='radio' id='pr_xbox_any' checked name='pr' value='any' />
								<img id='pr_xbox_any_img' src='{$httproot}/img/assets/icon/any.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='pr_xbox_off' name='pr' value='offense' />
								<img id='pr_xbox_off_img' src='{$httproot}/img/assets/icon/offense.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='pr_xbox_def' name='pr' value='defense' />
								<img id='pr_xbox_def_img' src='{$httproot}/img/assets/icon/defense.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='pr_xbox_tank' name='pr' value='tank' />
								<img id='pr_xbox_tank_img' src='{$httproot}/img/assets/icon/tank.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='pr_xbox_supp' name='pr' value='support' />
								<img id='pr_xbox_supp_img' src='{$httproot}/img/assets/icon/support.png'>
							</label>
						</div>
					</div>
					<div class='form-group'  id='sr_xbox' style='display:none'>
						<label class='control-label col-sm-2 role-info' for='sr'>Secondary role <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-10'>
							<label class='radio-img'>
								<input type='radio' id='sr_xbox_any' checked name='sr' value='any' />
								<img id='sr_xbox_any_img' src='{$httproot}/img/assets/icon/any.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='sr_xbox_off' name='sr' value='offense' />
								<img id='sr_xbox_off_img' src='{$httproot}/img/assets/icon/offense.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='sr_xbox_def' name='sr' value='defense' />
								<img id='sr_xbox_def_img' src='{$httproot}/img/assets/icon/defense.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='sr_xbox_tank' name='sr' value='tank' />
								<img id='sr_xbox_tank_img' src='{$httproot}/img/assets/icon/tank.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='sr_xbox_supp' name='sr' value='support' />
								<img id='sr_xbox_supp_img' src='{$httproot}/img/assets/icon/support.png'>
							</label>
						</div>
					</div>
					<div class='form-group'>
						<label class='control-label col-sm-2 mic-info' for='have_mic'>Have Mic <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-4'>
							<label class='radio-img'>
								<input type='radio' name='have_mic' value='1' />
								<img src='{$httproot}/img/assets/icon/mic-yes.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' checked name='have_mic' value='0' />
								<img src='{$httproot}/img/assets/icon/mic-no.png'>
							</label>
						</div>
						<label class='control-label col-sm-2 mic-req-info' for='mic_req'>Require Mic <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-4'>
							<label class='radio-img'>
								<input type='radio' name='mic_req' value='1' />
								<img src='{$httproot}/img/assets/icon/yes.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' checked name='mic_req' value='0' />
								<img src='{$httproot}/img/assets/icon/no.png'>
							</label>
						</div>
					</div>
					<input type='hidden' name='platform' value='xbox' />
					<div class='form-group'>
						<div class='col-sm-12 ' align='center'>
							<button type='submit' class='btn btn-xs btn-default' >Submit</button>
						</div>
					</div>
				</form>
			</div>
			<div class='col-sm-12' id='ps4_form_div' style='display:none'>
				<h2 align='center'>Join Group on Playstation 4</h2>
				<form class='form-horizontal' role='form' id='joinps4' method='POST' action=''>
					<div class='form-group'>
						<label class='control-label col-sm-2' for='gamer_tag'>PSN Username:</label>
						<div class='col-sm-10'>
							<input type='text' class='form-control' id='gamer_tag' name='gamer_tag' placeholder='YourPsnName'>
						</div>
					</div>
					<div class='form-group'>
						<label class='control-label col-sm-2 lead-info' for='lead'>Will you lead <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-10'>
							<label class='radio-img'>
								<input type='radio' checked name='lead' value='1' />
								<img src='{$httproot}/img/assets/icon/yes.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' name='lead' value='0' />
								<img src='{$httproot}/img/assets/icon/no.png'>
							</label>
						</div>
					</div>
					<div class='form-group'>
						<label class='control-label col-sm-2 game-type-info' for='game_type'>Game type <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-10'>
							<label class='radio-img'>
								<input type='radio' checked name='game_type' value='quick' />
								<img src='{$httproot}/img/assets/icon/quick.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' name='game_type' value='competitive' />
								<img src='{$httproot}/img/assets/icon/competitive.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' name='game_type' value='custom' />
								<img src='{$httproot}/img/assets/icon/custom.png'>
							</label>
						</div>
					</div>
					<div class='form-group' id='pr_ps4'>
						<label class='control-label col-sm-2 role-info' for='pr'>Primary role <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-10'>
							<label class='radio-img'>
								<input type='radio' id='pr_ps4_any' checked name='pr' value='any' />
								<img id='pr_ps4_any_img' src='{$httproot}/img/assets/icon/any.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='pr_ps4_off' name='pr' value='offense' />
								<img id='pr_ps4_off_img' src='{$httproot}/img/assets/icon/offense.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='pr_ps4_def' name='pr' value='defense' />
								<img id='pr_ps4_def_img' src='{$httproot}/img/assets/icon/defense.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='pr_ps4_tank' name='pr' value='tank' />
								<img id='pr_ps4_tank_img' src='{$httproot}/img/assets/icon/tank.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='pr_ps4_supp' name='pr' value='support' />
								<img id='pr_ps4_supp_img' src='{$httproot}/img/assets/icon/support.png'>
							</label>
						</div>
					</div>
					<div class='form-group'  id='sr_ps4' style='display:none'>
						<label class='control-label col-sm-2 role-info' for='sr'>Secondary role <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-10'>
							<label class='radio-img'>
								<input type='radio' id='sr_ps4_any' checked name='sr' value='any' />
								<img id='sr_ps4_any_img' src='{$httproot}/img/assets/icon/any.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='sr_ps4_off' name='sr' value='offense' />
								<img id='sr_ps4_off_img' src='{$httproot}/img/assets/icon/offense.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='sr_ps4_def' name='sr' value='defense' />
								<img id='sr_ps4_def_img' src='{$httproot}/img/assets/icon/defense.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='sr_ps4_tank' name='sr' value='tank' />
								<img id='sr_ps4_tank_img' src='{$httproot}/img/assets/icon/tank.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' id='sr_ps4_supp' name='sr' value='support' />
								<img id='sr_ps4_supp_img' src='{$httproot}/img/assets/icon/support.png'>
							</label>
						</div>
					</div>
					<div class='form-group'>
						<label class='control-label col-sm-2 mic-info' for='have_mic'>Have Mic <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-4'>
							<label class='radio-img'>
								<input type='radio' name='have_mic' value='1' />
								<img src='{$httproot}/img/assets/icon/mic-yes.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' checked name='have_mic' value='0' />
								<img src='{$httproot}/img/assets/icon/mic-no.png'>
							</label>
						</div>
						<label class='control-label col-sm-2 mic-req-info' for='mic_req'>Require Mic <i class='fa fa-question-circle'></i>:</label>
						<div class='col-sm-4'>
							<label class='radio-img'>
								<input type='radio' name='mic_req' value='1' />
								<img src='{$httproot}/img/assets/icon/yes.png'>
							</label>
							<label class='radio-img'>
								<input type='radio' checked name='mic_req' value='0' />
								<img src='{$httproot}/img/assets/icon/no.png'>
							</label>
						</div>
					</div>
					<input type='hidden' name='platform' value='ps4' />
					<div class='form-group'>
						<div class='col-sm-12 ' align='center'>
							<button type='submit' class='btn btn-xs btn-default'>Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	";
}

	function login_form(){
		echo "
	<div class='row'>
		<div class='col-sm-12'>
			<h2 align='center'>Have Access?</h2>
			<form class='form-horizontal' role='form' id='login' method='POST' action=''>
				<div class='form-group'>
					<label class='control-label col-sm-2' for='email'>Email:</label>
					<div class='col-sm-10'>
						<input type='email' class='form-control' id='email' name='email' placeholder='you@wherever.com'>
					</div>
				</div>
				<div class='form-group'>
					<div class='col-sm-12 ' align='center'>
						<button type='submit' class='btn btn-xs btn-default' >Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	";
}

function add_user_form(){
	echo "
<div class='row'>
	<div class='col-sm-12'>
		<h2 align='center'>Add User</h2>
		<form class='form-horizontal' role='form' id='add' method='POST' action=''>
			<div class='form-group'>
				<label class='control-label col-sm-2' for='email'>Email:</label>
				<div class='col-sm-10'>
					<input type='email' class='form-control' id='email' name='email' placeholder='you@wherever.com'>
				</div>
			</div>
			<div class='form-group'>
				<div class='col-sm-12 ' align='center'>
					<button type='submit' class='btn btn-xs btn-default' >Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>
	";
}

function send_link_form(){
	echo "
<div class='row'>
	<div class='col-sm-12'>
		<h2 align='center'>Send Link</h2>
		<form class='form-horizontal' role='form' id='link' method='POST' action=''>
			<div class='form-group'>
				<label class='control-label col-sm-2' for='phone'>Phone:</label>
				<div class='col-sm-10'>
					<input type='phone' class='form-control' id='phone' name='phone' placeholder='123-456-7890'>
				</div>
			</div>
			<div class='form-group'>
				<div class='col-sm-12 ' align='center'>
					<button type='submit' class='btn btn-xs btn-default' >Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>
	";
}

function modals($trigger){
	switch($trigger){
		case "all":
			?>
			<div class="modal fade info-modal-sm" tabindex="-1" id='modal-small' role="dialog" aria-labelledby="modalSmallTitle">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="modalSmallTitle"></h4>
						</div>
						<div class="modal-body" id="modalSmallBody">
							
						</div>
						<div class="modal-footer" id="modalSmallFooter">
							<button type="button" id="modalSmallCloseBtn" class="btn btn-default" data-dismiss="modal">Ok</button>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade info-modal" tabindex="-1" id='modal-normal' role="dialog" aria-labelledby="modalTitle">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="modalTitle"></h4>
						</div>
						<div class="modal-body" id="modalBody">
							
						</div>
						<div class="modal-footer" id="modalFooter">
							<button type="button" id="modalCloseBtn" class="btn btn-default" data-dismiss="modal">Ok</button>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade info-modal-lg" tabindex="-1" id='modal-large' role="dialog" aria-labelledby="modalLargeTitle">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="modalLargeTitle"></h4>
						</div>
						<div class="modal-body" id="modalLargeBody">
							
						</div>
						<div class="modal-footer" id="modalLargeFooter">
							<button type="button" id="modalLargeCloseBtn" class="btn btn-default" data-dismiss="modal">Ok</button>
						</div>
					</div>
				</div>
			</div>
			<?php
		break;
		case "contact":
			
			break;
		default:
			?>
			<div class="modal fade info-modal-sm" tabindex="-1" id='modal-small' role="dialog" aria-labelledby="modalSmallTitle">
					<div class="modal-dialog modal-sm">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="modalSmallTitle"></h4>
							</div>
							<div class="modal-body" id="modalSmallBody">
								
							</div>
							<div class="modal-footer" id="modalSmallFooter">
								<button type="button" id="modalSmallCloseBtn" class="btn btn-default" data-dismiss="modal">Ok</button>
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade info-modal" tabindex="-1" id='modal-normal' role="dialog" aria-labelledby="modalTitle">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="modalTitle"></h4>
							</div>
							<div class="modal-body" id="modalBody">
								
							</div>
							<div class="modal-footer" id="modalFooter">
								<button type="button" id="modalCloseBtn" class="btn btn-default" data-dismiss="modal">Ok</button>
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade info-modal-lg" tabindex="-1" id='modal-large' role="dialog" aria-labelledby="modalLargeTitle">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="modalLargeTitle"></h4>
							</div>
							<div class="modal-body" id="modalLargeBody">
								
							</div>
							<div class="modal-footer" id="modalLargeFooter">
								<button type="button" id="modalLargeCloseBtn" class="btn btn-default" data-dismiss="modal">Ok</button>
							</div>
						</div>
					</div>
				</div>
			<?php
	}
}

?>