<?php
function set_debug(){
if ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false){
	define('DEBUG', true);
} else {
	define('DEBUG', false);
}
// you want all errors to be triggered
error_reporting(E_ALL); 

if(DEBUG == true)
{
		// you're developing, so you want all errors to be shown
		ini_set('display_errors','on');
		// logging is usually overkill during dev
		ini_set('log_errors','on'); 
}
else
{
		// you don't want to display errors on a prod environment
		ini_set('display_errors','off');
		// you definitely wanna log any occurring
		ini_set('log_errors','on'); 
}
}

function is_session_started()
{
    if ( php_sapi_name() !== 'cli' ) {
        if ( version_compare(phpversion(), '5.4.0', '>=') ) {
            return session_status() === PHP_SESSION_ACTIVE ? TRUE : FALSE;
        } else {
            return session_id() === '' ? FALSE : TRUE;
        }
    }
    return FALSE;
}

function check_access(){
	if(isset($_SESSION['allowed'])){
		$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
		$db = new Database();
		$db->query("SELECT id FROM `access_list` WHERE `key` = :key");
		$db->bind(":key",hash('sha256', $_SESSION['allowed']['u'].$_SESSION['allowed']['salt']));
		if ($db->single()){
			
			return true;
		} else {
			
			return false;
		}
	} else {
		
		return false;
	}
	
}

function queries($reg, $plat, $pr, $sr, $hm, $mr, $t){
	$offRole = array(0 => "offense", 1 => "defense", 2 => "tank", 3 =>"support");
	foreach ($offRole as $key =>$value){
		if($value == $pr){
			array_splice($offRole, $key, 1);
		} 
	};
	foreach ($offRole as $key =>$value){
		if($value == $sr){
			array_splice($offRole, $key, 1);
		} 
	};
	$query = array();
	if ($pr == "any"){
		$query[0] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 ";
	} else {
		if ($sr == "any"){
			$query[0] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `need_$pr` = 1 ";
			$query[1] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `$pr` <= 1 AND `$offRole[0]` >= 1 AND `$offRole[1]` >= 1 ".($offRole[2] ? "AND `".$offRole[2]."` >= 1 " : '');
			$query[2] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `$pr` <= 1 AND `$offRole[0]` >= 1 ";
			$query[3] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `$pr` <= 1 AND `$offRole[1]` >= 1 ";
			$query[4] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 ";
		} else {
			$query[0] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `need_$pr` = 1 ";
			$query[1] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `need_$sr` = 1 AND `$pr` < 2 AND `$sr` < 2 ";
			$query[2] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `$pr` <= 1 AND `$offRole[0]` >= 1 AND `$offRole[1]` >= 1 ";
			$query[3] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `$sr` <= 1 AND `$offRole[0]` >= 1 AND `$offRole[1]` >= 1 ";
			$query[4] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `$pr` <= 1 AND `$offRole[0]` >= 1 ";
			$query[5] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `$pr` <= 1 AND `$offRole[1]` >= 1 ";
			$query[6] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `$sr` <= 1 AND `$offRole[0]` >= 1 ";
			$query[7] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `$sr` <= 1 AND `$offRole[1]` >= 1 ";
			$query[8] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `$pr` <= 1 AND $pr < 2 AND $sr < 2 ";
			$query[9] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `$sr` <= 1 AND $pr < 2 AND $sr < 2 ";
			$query[10] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `$pr` <= 1 ";
			$query[11] = "SELECT id, kicked, group_token, p2, p3, p4, p5, p6 FROM `groups_{$reg}_{$plat}` WHERE `open_count` > 0 AND `$sr` <= 1 ";
		}
	}
	
	$micop = "";
	if($mr == 1){
		$micop = "AND `mic_req` = 1";
	};
	if($hm == 0){
		$micop = "AND `mic_req` != 1";
	};
	
	$i = 0;
	foreach($query as $k => $q){
		$query[$i] = $q.$micop." AND `game_type` = '$t' ORDER BY `last_dropped` DESC";
		$i++;
	};
	return $query;
}

function option_array($reg,$plat,$gt,$l,$gametype,$pr,$sr,$hm,$mr) {
	$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
	
	$opts = array();
	switch($plat){
		case "pc":
			$opts['plat'] = $plat;
			break;
		case "xbox":
			$opts['plat'] = $plat;
			break;
		case "ps4":
			$opts['plat'] = $plat;
			break;
		default:
			$opts['plat'] = "us";
	}
	switch($reg){
		case "us":
			$opts['reg'] = $reg;
			break;
		case "eu":
			$opts['reg'] = $reg;
			break;
		case "as":
			$opts['reg'] = $reg;
			break;
		default:
			$opts['reg'] = "us";
	}
	if ($opts['plat'] =="pc"){
			if(Respect\Validation\Validator::alnum("#")->length(8,17)->validate($gt) && strpos($gt, '#') !== false){
				$opts['gt'] = $gt;
			} else {
				$opts['gt'] = "badtag";
			}
		}
		if ($opts['plat'] =="xbox"){
			if(Respect\Validation\Validator::alnum()->length(3,15)->validate($gt)){
				$opts['gt'] = $gt;
			} else {
				$opts['gt'] = "badtag";
			}
		}
		if ($opts['plat'] =="ps4"){
			if(Respect\Validation\Validator::alnum("-_")->length(3,16)->validate($gt)){
				$opts['gt'] = $gt;
			} else {
				$opts['gt'] = "badtag";
			}
		}
	if ($l == 0 || 1){
		$opts['lead'] = $l;
	} else {
		$opts['lead'] = 0;
	};
	if ($gametype == "quick" || "competitive" || "custom"){
		$opts['type'] = $gametype;
	} else {
		$opts['type'] = "quick";
	};
	if ($pr == "all" || "offense" || "defense" || "tank" || "support"){
		$opts['pr'] = $pr;
	} else {
		$opts['pr'] = "all";
	};
	if ($sr == "all" || "offense" || "defense" || "tank" || "support"){
		$opts['sr'] = $sr;
	} else {
		$opts['sr'] = "all";
	};
	if ($hm == 0 || 1){
		$opts['have_mic'] = $hm;
	} else {
		$opts['have_mic'] = 0;
	};
	if ($mr == 0 || 1){
		$opts['mic_req'] = $mr;
	} else {
		$opts['mic_req'] = 0;
	};
	return $opts;
}

function minify_html($input) {
    if(trim($input) === "") return $input;
    // Remove extra white-space(s) between HTML attribute(s)
    $input = preg_replace_callback('#<([^\/\s<>!]+)(?:\s+([^<>]*?)\s*|\s*)(\/?)>#s', function($matches) {
        return '<' . $matches[1] . preg_replace('#([^\s=]+)(\=([\'"]?)(.*?)\3)?(\s+|$)#s', ' $1$2', $matches[2]) . $matches[3] . '>';
    }, str_replace("\r", "", $input));
    // Minify inline CSS declaration(s)
    if(strpos($input, ' style=') !== false) {
        $input = preg_replace_callback('#<([^<]+?)\s+style=([\'"])(.*?)\2(?=[\/\s>])#s', function($matches) {
            return '<' . $matches[1] . ' style=' . $matches[2] . minify_css($matches[3]) . $matches[2];
        }, $input);
    }
    return preg_replace(
        array(
            // t = text
            // o = tag open
            // c = tag close
            // Keep important white-space(s) after self-closing HTML tag(s)
            '#<(img|input)(>| .*?>)#s',
            // Remove a line break and two or more white-space(s) between tag(s)
            '#(<!--.*?-->)|(>)(?:\n*|\s{2,})(<)|^\s*|\s*$#s',
            '#(<!--.*?-->)|(?<!\>)\s+(<\/.*?>)|(<[^\/]*?>)\s+(?!\<)#s', // t+c || o+t
            '#(<!--.*?-->)|(<[^\/]*?>)\s+(<[^\/]*?>)|(<\/.*?>)\s+(<\/.*?>)#s', // o+o || c+c
            '#(<!--.*?-->)|(<\/.*?>)\s+(\s)(?!\<)|(?<!\>)\s+(\s)(<[^\/]*?\/?>)|(<[^\/]*?\/?>)\s+(\s)(?!\<)#s', // c+t || t+o || o+t -- separated by long white-space(s)
            '#(<!--.*?-->)|(<[^\/]*?>)\s+(<\/.*?>)#s', // empty tag
            '#<(img|input)(>| .*?>)<\/\1\x1A>#s', // reset previous fix
            '#(&nbsp;)&nbsp;(?![<\s])#', // clean up ...
            // Force line-break with `&#10;` or `&#xa;`
            '#&\#(?:10|xa);#',
            // Force white-space with `&#32;` or `&#x20;`
            '#&\#(?:32|x20);#',
            // Remove HTML comment(s) except IE comment(s)
            '#\s*<!--(?!\[if\s).*?-->\s*|(?<!\>)\n+(?=\<[^!])#s'
        ),
        array(
            "<$1$2</$1\x1A>",
            '$1$2$3',
            '$1$2$3',
            '$1$2$3$4$5',
            '$1$2$3$4$5$6$7',
            '$1$2$3',
            '<$1$2',
            '$1 ',
            "\n",
            ' ',
            ""
        ),
    $input);
}

function minify_css($input) {
    if(trim($input) === "") return $input;
    // Force white-space(s) in `calc()`
    if(strpos($input, 'calc(') !== false) {
        $input = preg_replace_callback('#(?<=[\s:])calc\(\s*(.*?)\s*\)#', function($matches) {
            return 'calc(' . preg_replace('#\s+#', "\x1A", $matches[1]) . ')';
        }, $input);
    }
    return preg_replace(
        array(
            // Remove comment(s)
            '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')|\/\*(?!\!)(?>.*?\*\/)|^\s*|\s*$#s',
            // Remove unused white-space(s)
            '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/))|\s*+;\s*+(})\s*+|\s*+([*$~^|]?+=|[{};,>~+]|\s*+-(?![0-9\.])|!important\b)\s*+|([[(:])\s++|\s++([])])|\s++(:)\s*+(?!(?>[^{}"\']++|"(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')*+{)|^\s++|\s++\z|(\s)\s+#si',
            // Replace `0(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)` with `0`
            '#(?<=[\s:])(0)(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)#si',
            // Replace `:0 0 0 0` with `:0`
            '#:(0\s+0|0\s+0\s+0\s+0)(?=[;\}]|\!important)#i',
            // Replace `background-position:0` with `background-position:0 0`
            '#(background-position):0(?=[;\}])#si',
            // Replace `0.6` with `.6`, but only when preceded by a white-space or `=`, `:`, `,`, `(`, `-`
            '#(?<=[\s=:,\(\-]|&\#32;)0+\.(\d+)#s',
            // Minify string value
            '#(\/\*(?>.*?\*\/))|(?<!content\:)([\'"])([a-z_][-\w]*?)\2(?=[\s\{\}\];,])#si',
            '#(\/\*(?>.*?\*\/))|(\burl\()([\'"])([^\s]+?)\3(\))#si',
            // Minify HEX color code
            '#(?<=[\s=:,\(]\#)([a-f0-6]+)\1([a-f0-6]+)\2([a-f0-6]+)\3#i',
            // Replace `(border|outline):none` with `(border|outline):0`
            '#(?<=[\{;])(border|outline):none(?=[;\}\!])#',
            // Remove empty selector(s)
            '#(\/\*(?>.*?\*\/))|(^|[\{\}])(?:[^\s\{\}]+)\{\}#s',
            '#\x1A#'
        ),
        array(
            '$1',
            '$1$2$3$4$5$6$7',
            '$1',
            ':0',
            '$1:0 0',
            '.$1',
            '$1$3',
            '$1$2$4$5',
            '$1$2$3',
            '$1:0',
            '$1$2',
            ' '
        ),
    $input);
}
// JavaScript Minifier
function minify_js($input) {
    if(trim($input) === "") return $input;
    return preg_replace(
        array(
            // Remove comment(s)
            '#\s*("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')\s*|\s*\/\*(?!\!|@cc_on)(?>[\s\S]*?\*\/)\s*|\s*(?<![\:\=])\/\/.*(?=[\n\r]|$)|^\s*|\s*$#',
            // Remove white-space(s) outside the string and regex
            '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/)|\/(?!\/)[^\n\r]*?\/(?=[\s.,;]|[gimuy]|$))|\s*([!%&*\(\)\-=+\[\]\{\}|;:,.<>?\/])\s*#s',
            // Remove the last semicolon
            '#;+\}#',
            // Minify object attribute(s) except JSON attribute(s). From `{'foo':'bar'}` to `{foo:'bar'}`
            '#([\{,])([\'])(\d+|[a-z_]\w*)\2(?=\:)#i',
            // --ibid. From `foo['bar']` to `foo.bar`
            '#([\w\)\]])\[([\'"])([a-z_]\w*)\2\]#i',
            // Replace `true` with `!0`
            '#(?<=return |[=:,\(\[])true\b#',
            // Replace `false` with `!1`
            '#(?<=return |[=:,\(\[])false\b#',
            // Clean up ...
            '#\s*(\/\*|\*\/)\s*#'
        ),
        array(
            '$1',
            '$1$2',
            '}',
            '$1$3',
            '$1.$3',
            '!0',
            '!1',
            '$1'
        ),
    $input);
}

?>