<?php
function reset_data(){
	is_session_started();
	if(isset($_SESSION['data']['group']))leave_group($_SESSION['data']);
	session_destroy();
	$rdata['state'] = "success";
	echo json_encode($rdata);
}

function promote_to_lead($c, $newLeadPos, $newLead) {
	$time = time();
	$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
	$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"c:/xampp/htdocs/overwatchmm/www":"";
	$rdata = array();
	$db = new Database();
	$q = "SELECT id, p1, p1t, p1r1, p1r2, p1m, $newLeadPos, {$newLeadPos}t, {$newLeadPos}r1, {$newLeadPos}r2, {$newLeadPos}m FROM `groups_{$c['reg']}_{$c['plat']}` WHERE `$newLeadPos` = :player AND `leader_token` = :lt AND `group_token` = :gt";
	$db->query($q);
	$db->bind(":player",$newLead);
	$db->bind(":lt", $c['lt']);
	$db->bind(":gt", $c['group']);
	if($result = $db->single()){
		$factory = new RandomLib\Factory;
		$generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::MEDIUM));
		$newLeaderToken = $generator->generateString(64, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
		$p1new = $result["{$newLeadPos}"];
		$p1tnew = $result["{$newLeadPos}t"];
		$p1r1new = $result["{$newLeadPos}r1"];
		$p1r2new = $result["{$newLeadPos}r2"];
		$p1mnew = $result["{$newLeadPos}m"];
		$q = "UPDATE `groups_{$c['reg']}_{$c['plat']}` SET `leader_token` = :lt, `p1` = :p1, `p1t` = :p1t, `p1r1` = :p1r1, `p1r2` = :p1r2, `p1m` = :p1m, `{$newLeadPos}` = :oldp1, `{$newLeadPos}t` = :oldp1t, `{$newLeadPos}r1` = :oldp1r1, `{$newLeadPos}r2` = :oldp1r2, `{$newLeadPos}m` = :oldp1m, `last_active` = $time WHERE `id` = :id";
		$db->query($q);
		$db->bind(":lt",$newLeaderToken);
		$db->bind(":p1", $p1new);
		$db->bind(":p1t", $p1tnew);
		$db->bind(":p1r1", $p1r1new);
		$db->bind(":p1r2", $p1r2new);
		$db->bind(":p1m", $p1mnew);
		$db->bind(":oldp1", $result["p1"]);
		$db->bind(":oldp1t", $result["p1t"]);
		$db->bind(":oldp1r1", $result["p1r1"]);
		$db->bind(":oldp1r2", $result["p1r2"]);
		$db->bind(":oldp1m", $result["p1m"]);
		$db->bind(":id",$result["id"]);
		if ($db->execute()){
			$time = time();
			$db->query("INSERT INTO `new_lead` (`player_token`, `new_leader_token`, `group`, `time`) VALUES (:pt, :nlt, :g, :t)");
			$db->bind(":pt", $result["{$newLeadPos}t"]);
			$db->bind(":nlt",$newLeaderToken);
			$db->bind(":g",$c['group']);
			$db->bind(":t",$time);
			unset($_SESSION['data']['lt']);
			$_SESSION['data']['pos'] = $newLeadPos;
			if($db->execute()){
				$rdata['state'] = "success";
				promote_to_lead_json($c, $newLeadPos, $p1tnew);
				echo json_encode($rdata);
			}
		}
	}
	
}

function promote_to_lead_json($c, $newLeadPos, $newLeadPt) {
	$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
	$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"c:/xampp/htdocs/overwatchmm/www":"";
	$jsonString = file_get_contents("{$root}/groups/{$c['group']}.json");
	$data = json_decode($jsonString, true);
	$p1 = $data['players']['p1']['gt'];
	$p1r1 = $data['players']['p1']['pr'];
	$p1r2 = $data['players']['p1']['sr'];
	$p1m = $data['players']['p1']['m'];
	$newlead = $data['players'][$newLeadPos]['gt'];
	$data['players']['p1']['gt'] = $data['players'][$newLeadPos]['gt'];
	$data['players']['p1']['pr'] = $data['players'][$newLeadPos]['pr'];
	$data['players']['p1']['sr'] = $data['players'][$newLeadPos]['sr'];
	$data['players']['p1']['m'] = $data['players'][$newLeadPos]['m'];
	$data['game']['chat'][] = array("time" => date('H:i:s', time()),"player" => "SERVER", "color" => "#E0E0E0", "msg" => "{$newlead} has been promoted to group leader!");
	$data['players'][$newLeadPos]['gt'] = $p1;
	$data['players'][$newLeadPos]['pr'] = $p1r1;
	$data['players'][$newLeadPos]['sr'] = $p1r2;
	$data['players'][$newLeadPos]['m'] = $p1m;
	$data['game']['nl'] = $newLeadPos;
	$data['game']['nltp'] = substr($newLeadPt, 35, 8);
	$data['game']['msg'] = "<div class='text-center'><h2>You are the new leader!</h2><strong>Click the button below to show the leader controls</strong><p><button class='btn btn-large btn-info' id='get_lead'>Accept Lead</button></p></div>";
	$newJsonString = json_encode($data);
	file_put_contents("{$root}/groups/{$c['group']}.json", $newJsonString, LOCK_EX);
}

function leader_leave_json($pt, $newinf, $oldLead, $newLead, $group){
	$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
	$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"c:/xampp/htdocs/overwatchmm/www":"";
	$jsonString = file_get_contents("{$root}/groups/".$group.".json");
	$data = json_decode($jsonString, true);
	$data['game']['nl'] = $newLead;
	$data['game']['nltp'] = substr($pt, 35, 8);
	$data['game']['msg'] = "<div class='text-center'><h2>You are the new leader!</h2><strong>Click the button below to show the leader controls</strong><p><button class='btn btn-large btn-info' id='get_lead'>Accept Lead</button></p></div>";
	$data['players'][$oldLead]['gt'] = "";
	$data['players'][$oldLead]['pr'] = "";
	$data['players'][$oldLead]['sr'] = "";
	$data['players'][$oldLead]['m'] = "";
	$data['players'][$newLead]['gt'] = "";
	$data['players'][$newLead]['pr'] = "";
	$data['players'][$newLead]['sr'] = "";
	$data['players'][$newLead]['m'] = "";
	$data['players']["p1"]['gt'] = $newinf['gt'];
	$data['players']["p1"]['pr'] = $newinf['pr'];
	$data['players']["p1"]['sr'] = $newinf['sr'];
	$data['players']["p1"]['m'] = ($newinf['have_mic'] == 0 ? "No" : "Yes");
	$data['game']['chat'][] = array("time" => date('H:i:s', time()),"player" => "SERVER", "color" => "#E0E0E0", "msg" => "Group leader has left! {$newinf['gt']} has been promoted to group leader!");
	$newJsonString = json_encode($data);
	file_put_contents("{$root}/groups/".$group.'.json', $newJsonString, LOCK_EX);
	
}

function add_p_to_json($inf, $pnumber, $gtoken ){
	$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
	$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"c:/xampp/htdocs/overwatchmm/www":"";
	$jsonString = file_get_contents("{$root}/groups/".$gtoken.".json");
	$data = json_decode($jsonString, true);
	$data['players'][$pnumber]['gt'] = $inf['gt'];
	$data['players'][$pnumber]['pr'] = $inf['pr'];
	$data['players'][$pnumber]['sr'] = $inf['sr'];
	$data['players'][$pnumber]['m'] = ($inf['have_mic'] == 0 ? "No" : "Yes");
	$data['game']['chat'][] = array("time" => date('H:i:s', time()), "player" => "SERVER", "color" => "#E0E0E0", "msg" => "{$inf['gt']} has joined the group");
	$newJsonString = json_encode($data);
	file_put_contents("{$root}/groups/".$gtoken.'.json', $newJsonString, LOCK_EX);
}

function remove_p_from_json($pnumber, $gtoken){
	$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
	$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"c:/xampp/htdocs/overwatchmm/www":"";
	$jsonString = file_get_contents("{$root}/groups/".$gtoken.".json");
	$data = json_decode($jsonString, true);
	$player = $data['players'][$pnumber]['gt'];
	$data['game']['chat'][] = array("time" => date('H:i:s', time()), "player" => "SERVER", "color" => "#E0E0E0", "msg" => "{$player} has left the group");
	$data['players'][$pnumber]['gt'] = "";
	$data['players'][$pnumber]['pr'] = "";
	$data['players'][$pnumber]['sr'] = "";
	$data['players'][$pnumber]['m'] = "";
	$newJsonString = json_encode($data);
	file_put_contents("{$root}/groups/".$gtoken.'.json', $newJsonString, LOCK_EX);
}

function kick_p_from_json($pnumber, $pname, $ptoken, $gtoken){
	$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
	$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"c:/xampp/htdocs/overwatchmm/www":"";
	$jsonString = file_get_contents("{$root}/groups/".$gtoken.".json");
	$data = json_decode($jsonString, true);
	$data['game']['kicked'][$pname] = $ptoken;
	$data['players'][$pnumber]['gt'] = "";
	$data['players'][$pnumber]['pr'] = "";
	$data['players'][$pnumber]['sr'] = "";
	$data['players'][$pnumber]['m'] = "";
	$data['game']['chat'][] = array("time" => date('H:i:s', time()), "player" => "SERVER", "color" => "#E0E0E0", "msg" => "{$pname} has been kicked from the group");
	$newJsonString = json_encode($data);
	file_put_contents("{$root}/groups/".$gtoken.'.json', $newJsonString, LOCK_EX);
}

function add_p_message_json($pnumber, $gtoken, $message ){
	$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
	$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"c:/xampp/htdocs/overwatchmm/www":"";
	$jsonString = file_get_contents("{$root}/groups/".$gtoken.".json");
	$data = json_decode($jsonString, true);
	$data['players'][$pnumber]['message'] = $message;
	$newJsonString = json_encode($data);
	file_put_contents("{$root}/groups/".$gtoken.'.json', $newJsonString, LOCK_EX);
}

function check_for_lead(){
	$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
	$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"c:/xampp/htdocs/overwatchmm/www":"";
	if(isset($_SESSION['data'])) {
		if(isset($_SESSION['data'])) {
			$inf = $_SESSION['data'];
			if (array_key_exists('lt',$inf)){
				$lt = $inf['lt'];
			} else {
				return false;
			}
		} else {
			return false;
		}
		$db = new Database();
		$q = "SELECT id FROM `groups_{$inf['reg']}_{$inf['plat']}` WHERE `group_token` = :gtoken AND `leader_token` = :ltoken";
		$db->query($q);
		$db->bind(":gtoken",$inf['group']);
		$db->bind(":ltoken",$lt);
		if ($db->execute()){
			if ($db->rowcount() == 1){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return false;
	}
	
}

function get_lead($c){
	$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm":$_SERVER['DOCUMENT_ROOT'];
	$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"c:/xampp/htdocs/overwatchmm/www":"";
	$db = new Database();
	$db->query("SELECT id, new_leader_token FROM `new_lead` WHERE `group` = :group AND `player_token` = :pt");
	$db->bind(":group",$c['group']);
	$db->bind(":pt",$c['pt']);
	if($result = $db->single()){
		$c['lt'] = $result['new_leader_token'];
		$c['pos'] = "p1";
		$_SESSION['data'] = $c;
		$db->query("DELETE FROM `new_lead` WHERE `id` = :id");
		$db->bind(":id",$result['id']);
		$db->execute();
	}
	
}

function kick_player($c, $pn, $pname){
	$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm":$_SERVER['DOCUMENT_ROOT'];
	$time = time();
	$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"c:/xampp/htdocs/overwatchmm/www":"";
	$rdata = array();
	$db = new Database();
	$q = "SELECT {$pn}t, kicked FROM `groups_{$c['reg']}_{$c['plat']}` WHERE `group_token` = :group AND {$pn} = :player";
	$db->query($q);
	$db->bind(":group", $c['group']);
	$db->bind(":player", $pname);
	if ($results = $db->single()){
		if ($results['kicked'] == null){
			$kicked = $results["{$pn}t"];
		} else {
			$kicked = $results['kicked'].",".$results["{$pn}t"];
		}
		$q = "UPDATE `groups_{$c['reg']}_{$c['plat']}` SET `kicked` = :kicked, `{$pn}` = null, `{$pn}r1` = null,`{$pn}r2` = null,`{$pn}m` = null, `{$pn}t` = null, `open_count` = `open_count` + 1, `last_active` = $time WHERE `group_token` = :group AND `leader_token` = :lt";
		$db->query($q);
		$db->bind(":group", $c['group']);
		$db->bind(":lt",$c['lt']);
		$db->bind(":kicked",$kicked);
		if($db->execute()){
			kick_p_from_json($pn, $pname, substr($results["{$pn}t"],22,16), $c['group']);
			$rdata['message'] = "<div class='alert alert-success fade in out'><a href='#' class='close' data-dismiss='alert' aria-label='close'><i class='fa fa-times-circle fa-lg fa-fw' aria-hidden='true'></i></a>Player has been removed from the group.<div>";
			echo json_encode($rdata);
		} else {
			$rdata['message'] = "<div class='alert alert-danger fade in out'><a href='#' class='close' data-dismiss='alert' aria-label='close'><i class='fa fa-times-circle fa-lg fa-fw' aria-hidden='true'></i></a>Something went wrong!.<div>";
			echo json_encode($rdata);
		}
	} else {
		$rdata['message'] = "<div class='alert alert-danger fade in out'><a href='#' class='close' data-dismiss='alert' aria-label='close'><i class='fa fa-times-circle fa-lg fa-fw' aria-hidden='true'></i></a>Something went wrong!.<div>";
			echo json_encode($rdata);
	}
	
}

?>