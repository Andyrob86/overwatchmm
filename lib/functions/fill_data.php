<?php
include '../../includes.php';
$data = file_get_contents('dummy_data2.json');
$data = json_decode($data);
$fields_string = "";
$i = 0;
foreach ($data as $key => $value){
	$w = rand(0.1,2);
	sleep($w);
	if($i >=2){
		break;
	}
	switch($value->game_type){
		case 0:
			$gt = "quick";
			break;
		case 1:
			$gt = "competitive";
			break;
		case 2:
			$gt = "custom";
			break;
		default:
			$gt = "quick";
			break;
	}
	switch($value->pr){
		case 0:
			$pr = "offense";
			break;
		case 1:
			$pr = "defense";
			break;
		case 2:
			$pr = "tank";
			break;
		case 3:
			$pr = "support";
			break;
		case 2:
			$pr = "any";
			break;
		default:
			$pr = "any";
			break;
	}
	switch($value->sr){
		case 0:
			$sr = "offense";
			break;
		case 1:
			$sr = "defense";
			break;
		case 2:
			$sr = "tank";
			break;
		case 3:
			$sr = "support";
			break;
		case 2:
			$sr = "any";
			break;
		default:
			$sr = "any";
			break;
	}
	if($pr == "any"){
		$sr = "any";
	}
	
	$data = array();
	$inf = option_array($value->lead, "quick", $pr, $sr, $value->have_mic, $value->mic_req);
	$inf['gt'] = $value->gamer_tag;
	$inf['queries'] = queries($inf['pr'],$inf['sr'],$inf['have_mic'],$inf['mic_req'],$inf['type']);
	$db = new Database();
	$db->query("SELECT id from `groups`");
	$db->execute();
	if ($db->rowcount()==0){
		if ($inf['lead'] == 0){
			$data['message'] = offer_lead(2);
			$data['state'] = "error";
			echo json_encode($data);
		};
		if ($inf['lead'] == 1) {
			create_group($inf);
		};
	} else {
		find_empty_group($inf);
	}
	$i++;
}

/*
$url = '../../overwatchqhandler.php';
$data = file_get_contents('dummy_data.json');
$data = json_decode($data);
$fields_string = "";
$i = 0;
foreach ($data as $key => $value){
	if($i >=10){
		break;
	}
	switch($value->game_type){
		case 0:
			$gt = "quick";
			break;
		case 1:
			$gt = "competitive";
			break;
		case 2:
			$gt = "custom";
			break;
		default:
			$gt = "quick";
			break;
	}
	switch($value->pr){
		case 0:
			$pr = "offense";
			break;
		case 1:
			$pr = "defense";
			break;
		case 2:
			$pr = "tank";
			break;
		case 3:
			$pr = "support";
			break;
		case 2:
			$pr = "any";
			break;
		default:
			$pr = "any";
			break;
	}
	switch($value->sr){
		case 0:
			$sr = "offense";
			break;
		case 1:
			$sr = "defense";
			break;
		case 2:
			$sr = "tank";
			break;
		case 3:
			$sr = "support";
			break;
		case 2:
			$sr = "any";
			break;
		default:
			$sr = "any";
			break;
	}
	if($pr == "any"){
		$sr = "any";
	}
	
	$fields = array(
		'gamer_tag' => urlencode($value->gamer_tag),
		'game_type' => urlencode($gt),
		'lead' => urlencode($value->lead),
		'pr' => urlencode($pr),
		'sr' => urlencode($sr),
		'have_mic' => urlencode($value->have_mic),
		'mic_req' => urlencode($value->mic_req),
		'platform' => "pc"
	);

	//url-ify the data for the POST
	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	rtrim($fields_string, '&');

	//open connection
	$ch = curl_init();

	//set the url, number of POST vars, POST data
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//execute post
	$result = curl_exec($ch);

	//close connection
	curl_close($ch);
	$i++;
	print_r($result);
}

*/
?>