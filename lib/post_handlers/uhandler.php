<?php
$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
include "{$root}/../vendor/autoload.php";
set_debug();
use Respect\Validation\Validator as v;
if ( is_session_started() === FALSE ) {
	$session = new session();
	$session->start_session('_s', true);
}
if (isset($_POST['reset']) && $_POST['reset'] == 1){
	reset_data();
}

if (isset($_POST['purge']) && $_POST['purge'] == 1){
	if(isset($_POST['purge1']) && $_POST['purge1'] == 1){
		$_SESSION['data']['group'] = null;
		$rdata["state"] = "success";
		echo json_encode($rdata);
	}
}

if (isset($_POST['get_lead']) && $_POST['get_lead'] == 1){
	get_lead($_SESSION['data']);
}

?>