<?php
$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"c:/xampp/htdocs/overwatchmm/www":"";
include "{$root}/../vendor/autoload.php";
set_debug();
use Respect\Validation\Validator as v;
$db = new Database();
if ( is_session_started() === FALSE ) {
	$session = new session();
	$session->start_session('_s', true);
}

function moveElement(&$array, $a, $b) {
    $out = array_splice($array, $a, 1);
    array_splice($array, $b, 0, $out);
}

if(isset($_POST['cycle-queue']) && $_POST['cycle-queue'] == 1){
		$S = $_SESSION['cust'];
		$db->query("SELECT * FROM `cust_groups` WHERE `group` = :g");
		$db->bind(":g",$S['group']);
		if($results = $db->single()){
			if ($S['token'] == $results['leader']){
				$jsonString = file_get_contents("{$root}/group_manager/".$S['group'].".json");
				$json = json_decode($jsonString, true);
				$cycled = $json['players'][1];
				unset($json['players'][1]);
				$json['players'] = array_values($json['players']);
				$json['players'][] = $cycled;
				$newJsonString = json_encode($json);
				file_put_contents("{$root}/group_manager/".$S['group'].'.json', $newJsonString, LOCK_EX);
				$rdata['state'] = "success";
				echo json_encode($rdata);
			}
		}
}

if(isset($_POST['alter-queue']) && $_POST['alter-queue'] == 1 && isset($_POST['oldpos']) && isset($_POST['newpos'])){
	$v = v::numeric();
	$oldP = $_POST['oldpos'];
	$newP = $_POST['newpos'];
	if ($v->validate($oldP) && $v->validate($newP)){
		$S = $_SESSION['cust'];
		
		$db->query("SELECT * FROM `cust_groups` WHERE `group` = :g");
		$db->bind(":g",$S['group']);
		if($results = $db->single()){
			if ($S['token'] == $results['leader']){
				$jsonString = file_get_contents("{$root}/group_manager/".$S['group'].".json");
				$json = json_decode($jsonString, true);
				moveElement($json['players'],$oldP,$newP);
				if(isset($_POST['leadtransfer']) && $_POST['leadtransfer'] == 1){
					$newLead = $json['players'][0];
					$DBP = json_decode($results['players']);
					$newLeadToken = $DBP->$newLead;
					$db->query("UPDATE `cust_groups` SET `leader` = :nlt, `leader_name` = :nl, `players` = :dbp WHERE `group` = :g");
					$db->bind(":nlt",$newLeadToken);
					$db->bind(":nl",$newLead);
					$db->bind(":dbp",json_encode($DBP));
					$db->bind(":g",$S['group']);
					$db->execute();
				}
				$newJsonString = json_encode($json);
				file_put_contents("{$root}/group_manager/".$S['group'].'.json', $newJsonString, LOCK_EX);
				$rdata['state'] = "success";
				echo json_encode($rdata);
			}
		}
	}
}

if (isset($_POST['kicked']) && $_POST['kicked'] == 1 && isset($_POST['player']) && v::alnum("#-_")->length(3,18)->validate($_POST['player'])){
	unset($_SESSION['cust']['group']);
	$rdata['state'] = "success";
	$rdata['mbody'] = "You have been kicked from this group!";
	echo json_encode($rdata);
}

if (isset($_POST['queue-kick']) && $_POST['queue-kick'] == 1 && isset($_POST['player'])&& $_POST['player'] != ""){
	if (isset($_SESSION['cust']) && v::alnum("#-_")->length(3,18)->validate($_POST['player'])){
		$S = $_SESSION['cust'];
		$kicked = $_POST['player'];
		
		$db->query("SELECT * FROM `cust_groups` WHERE `group` = :g");
		$db->bind(":g",$S['group']);
		if($results = $db->single()){
			if ($S['token'] == $results['leader']){
				$jsonString = file_get_contents("{$root}/group_manager/".$S['group'].".json");
				$json = json_decode($jsonString, true);
				$DBP = json_decode($results['players']);
				$pos = array_search($kicked, $json['players']);
				unset($json['players'][$pos]);
				$json['players'] = array_values($json['players']);
				$json['kicked'][] = $kicked;
				unset($DBP->$kicked);
				$db->query("UPDATE `cust_groups` SET `players` = :dbp WHERE `group` = :g");
				$db->bind(":dbp",json_encode($DBP));
				$db->bind(":g",$S['group']);
				$db->execute();
				$newJsonString = json_encode($json);
				file_put_contents("{$root}/group_manager/".$S['group'].'.json', $newJsonString, LOCK_EX);
				$rdata['state'] = "success";
				echo json_encode($rdata);
			}
		};
	}
}

if (isset($_POST['queue-remove']) && $_POST['queue-remove'] == 1 && isset($_POST['player'])&& $_POST['player'] != ""){
	if (isset($_SESSION['cust']) && v::alnum("#-_")->length(3,18)->validate($_POST['player'])){
		$S = $_SESSION['cust'];
		$removed = $_POST['player'];
		
		$db->query("SELECT * FROM `cust_groups` WHERE `group` = :g");
		$db->bind(":g",$S['group']);
		if($results = $db->single()){
			if ($S['token'] == $results['leader']){
				$jsonString = file_get_contents("{$root}/group_manager/".$S['group'].".json");
				$json = json_decode($jsonString, true);
				$DBP = json_decode($results['players']);
				$pos = array_search($removed, $json['players']);
				unset($json['players'][$pos]);
				$json['players'] = array_values($json['players']);
				unset($DBP->$removed);
				$db->query("UPDATE `cust_groups` SET `players` = :dbp WHERE `group` = :g");
				$db->bind(":dbp",json_encode($DBP));
				$db->bind(":g",$S['group']);
				$db->execute();
				$newJsonString = json_encode($json);
				file_put_contents("{$root}/group_manager/".$S['group'].'.json', $newJsonString, LOCK_EX);
				$rdata['state'] = "success";
				echo json_encode($rdata);
			}
		};
	}
}

if (isset($_POST['leave_queue']) && $_POST['leave_queue'] == 1){
	if (isset($_SESSION['cust'])){
		$S = $_SESSION['cust'];
		
		$db->query("SELECT * FROM `cust_groups` WHERE `group` = :g");
		$db->bind(":g",$S['group']);
		if($results = $db->single()){
			$jsonString = file_get_contents("{$root}/group_manager/".$S['group'].".json");
			$json = json_decode($jsonString, true);
			$DBP = json_decode($results['players']);
			$pos = array_search($S['name'], $json['players']);
			unset($json['players'][$pos]);
			$json['players'] = array_values($json['players']);
			if ($pos == 0 || "0"){
				if(!isset($json['players'][0])){
					$db->query("DELETE FROM `cust_groups` WHERE `group` = :g");
					$db->bind(":g",$S['group']);
					$db->execute();
					$db->query("DELETE FROM `cg_shortlinks` WHERE `group` = :g");
					$db->bind(":g",$S['group']);
					$db->execute();
					$newJsonString = json_encode($json);
					unlink("{$root}/group_manager/".$S['group'].'.json');
				} else {
					$newLead = $json['players'][0];
					$newLeadToken = $DBP->$newLead;
					unset($DBP->$S['name']);
					$db->query("UPDATE `cust_groups` SET `leader` = :nlt, `leader_name` = :nl, `players` = :dbp WHERE `group` = :g");
					$db->bind(":nlt",$newLeadToken);
					$db->bind(":nl",$newLead);
					$db->bind(":dbp",json_encode($DBP));
					$db->bind(":g",$S['group']);
					$db->execute();
					$newJsonString = json_encode($json);
					file_put_contents("{$root}/group_manager/".$S['group'].'.json', $newJsonString, LOCK_EX);
				}
			} else {
				unset($DBP->$S['name']);
				$db->query("UPDATE `cust_groups` SET `players` = :dbp WHERE `group` = :g");
				$db->bind(":dbp",json_encode($DBP));
				$db->bind(":g",$S['group']);
				$db->execute();
				$newJsonString = json_encode($json);
				file_put_contents("{$root}/group_manager/".$S['group'].'.json', $newJsonString, LOCK_EX);
			}
			unset($_SESSION['cust']['group']);
			$rdata['state'] = "success";
			echo json_encode($rdata);
		} else {
			unset($_SESSION['cust']);
			$rdata['state'] = "success";
			echo json_encode($rdata);
		};
	}
}

if (isset($_POST['name']) && isset($_POST['create']) && $_POST['create'] == 1){
	if (v::alnum("#-_")->length(3,18)->validate($_POST['name'])){
		$sdata = array();
		$rdata = array();
		$players = array();
		$name = $_POST['name'];
		$factory = new RandomLib\Factory;
		$generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::MEDIUM));
		$playerToken = $generator->generateString(64, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
		$groupToken = $generator->generateString(32, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
		$shortLink = $generator->generateString(6, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
		$players = array($name => $playerToken);
		$json['kicked'] = "";
		$json['players'][] = $name;
		
		$db->query("INSERT INTO `cust_groups` (`group`, `leader`, `leader_name`, `players`) VALUES (:g, :l, :ln, :p)");
		$db->bind(":g",$groupToken);
		$db->bind(":l",$playerToken);
		$db->bind(":ln",$name);
		$db->bind(":p",json_encode($players));
		if($db->execute()){
			$db->query("INSERT INTO `cg_shortlinks` (`link`, `group`) VALUES (:sl, :g)");
			$db->bind(":sl",$shortLink);
			$db->bind(":g",$groupToken);
			if ($db->execute()){
				$id = $db->lastInsertId();
				$sdata['group'] = $groupToken;
				$sdata['token'] = $playerToken;
				$sdata['name'] = $name;
				$sdata['link'] = $id.$shortLink;
				$_SESSION['cust'] = $sdata;
				$fp = fopen("{$root}/group_manager/$groupToken.json", 'a');
				flock($fp, LOCK_EX);
				usleep(rand(20,80));
				fwrite($fp, json_encode($json));
				flock($fp, LOCK_UN);
				fclose($fp);
				$rdata['state'] = "success";
				$rdata['mtitle'] = "Success!";
				echo json_encode($rdata);
			}
		}
	} else {
		$rdata['state'] = "nameError";
		$rdata['mtitle'] = "Error";
		$rdata['mbody'] = "Please double check your name and make sure it is a valid Battle.net, Xbox Live, or Playstation Network name.";
		echo json_encode($rdata);
	}
}

if (isset($_POST['name']) && isset($_POST['group']) && isset($_POST['join']) && $_POST['join'] == 1){
	if (v::alnum("#-_")->length(3,18)->validate($_POST['name']) && v::alnum()->length(32)->validate($_POST['group'])){
		$sdata = array();
		$rdata = array();
		$players = array();
		$name = $_POST['name'];
		$group = $_POST['group'];
		$factory = new RandomLib\Factory;
		$generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::MEDIUM));
		$playerToken = $generator->generateString(64, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
		$players[$name] = $playerToken;
		$json['players'][] = $name;
		
		$db->query("SELECT players FROM `cust_groups` WHERE `group` = :g");
		$db->bind(":g",$group);
		if($result = $db->single()){
			$players = json_decode($result['players']);
			$match = "";
			$kicked = "";
			foreach ($players as $key => $value){
				if ($key == $name){
					$match = "found";
				}
			}
			$jsonString = file_get_contents("{$root}/group_manager/".$group.".json");
			$data = json_decode($jsonString, true);
			if ($data['kicked'] != "" && in_array($name,$data['kicked'])){
				$kicked = "kicked";
			}
			if ($match != "found" && $kicked != "kicked"){
				$players->$name = $playerToken;
				$db->query("UPDATE `cust_groups` SET `players` = :p WHERE `group` = :g");
				$db->bind(":p",json_encode($players));
				$db->bind(":g",$group);
				if($db->execute()){
					$sdata['group'] = $group;
					$sdata['token'] = $playerToken;
					$sdata['name'] = $name;
					$sdata['link'] = $_SESSION['link'];
					$_SESSION['cust'] = $sdata;
					$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
					$httproot = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"c:/xampp/htdocs/overwatchmm/www":"";
					$data['players'][] = $name;
					$newJsonString = json_encode($data);
					file_put_contents("{$root}/group_manager/".$group.'.json', $newJsonString, LOCK_EX);
					$rdata['state'] = "success";
					$rdata['mtitle'] = "Success!";
					echo json_encode($rdata);
				}
			} else {
				if($kicked == "kicked"){
					$rdata['mhead'] = "Kicked";
					$rdata['mbody'] = "You have been kicked from this group. You will not be able to rejoin it!";
				} else {
					$rdata['mhead'] = "Name Taken";
					$rdata['mbody'] = "That name is already in use in this group! You may need to talk to the group leader if you believe someone else is using your name.";
				}
				$rdata['state'] = "nameError";
				echo json_encode($rdata);
			}
		}
	} else {
		$rdata['state'] = "nameError";
		$rdata['mtitle'] = "Error";
		$rdata['mbody'] = "Please double check your name and make sure it is a valid Battle.net, Xbox Live, or Playstation Network name.";
		echo json_encode($rdata);
	}
}
?>