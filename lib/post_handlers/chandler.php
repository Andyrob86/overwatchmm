<?php
$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
include "{$root}/../vendor/autoload.php";
set_debug();
use Respect\Validation\Validator as v;
if ( is_session_started() === FALSE ) {
	$session = new session();
	$session->start_session('_s', true);
}
if ('php://input' !== null){
	$data = json_decode(file_get_contents('php://input'), true);
	if ($data['message'] != ""){
		$sgroup = $_SESSION['data']['group'];
		$sgt = $_SESSION['data']['gt'];
		$scolor = $_SESSION['data']['color'];
		$config = HTMLPurifier_Config::createDefault();
		$config->set('HTML.Allowed', '');
		$purifier = new HTMLPurifier($config);
		$message = $purifier->purify($data['message']);
		$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
		if (($message) != "\n" || "") {
			if (preg_match($reg_exUrl, $message, $url)) {
				$message = preg_replace($reg_exUrl, '<a class="chat-link" href="'.$url[0].'" target="_blank">'.$url[0].'</a>', $message);
			} 
		}
		$jsonString = file_get_contents("{$root}/groups/".$sgroup.".json");
		$data = json_decode($jsonString, true);
		$data['game']['chat'][] = array("time" => date('H:i:s', time()),"player" => $sgt, "color" => $scolor, "msg" => $message);
		$newJsonString = json_encode($data);
		file_put_contents("{$root}/groups/".$sgroup.'.json', $newJsonString, LOCK_EX);
		$rdata['time'] = date("H:i:s", time());
		$rdata['message'] = $message;
		$rdata['state'] = "success";
		echo json_encode($rdata);
	} else {
		$rdata['state'] = "error";
		echo json_encode($rdata);
	}
}

?>