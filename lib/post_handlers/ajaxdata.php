<?php
$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
include "{$root}/../vendor/autoload.php";
set_debug();
use Respect\Validation\Validator as v;
if ( is_session_started() === FALSE ) {
	$session = new session();
	$session->start_session('_s', true);
}
if(isset($_POST['get_data']) && $_POST['get_data'] == 1){
	if (isset($_SESSION['data']))echo json_encode($_SESSION['data']);
}

?>