<?php
$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
include "{$root}/../vendor/autoload.php";
set_debug();
use Respect\Validation\Validator as v;
if ( is_session_started() === FALSE ) {
	$session = new session();
	$session->start_session('_s', true);
}
if (isset($_POST['leave_group']) && $_POST['leave_group'] == 1){
	leave_group($_SESSION['data']);
}

if (isset($_POST['promote_leader']) && $_POST['promote_leader'] == 1 && isset($_POST['player_number']) && isset($_POST['player'])){
	if(v::alnum("#-_")->length(1,17)->validate($_POST['player']) && v::alnum("")->length(2)->validate($_POST['player_number'])) {
		promote_to_lead($_SESSION['data'], $_POST['player_number'], $_POST['player']);
	}
}

if (isset($_POST['kick_player']) && isset($_POST['player']) && $_POST['kick_player'] == 1 && isset($_POST['player_name'])){
	if (v::alnum()->length(2)->validate($_POST['player']) && v::alnum("#-_")->length(1,17)->validate($_POST['player_name'])){
		kick_player($_SESSION['data'], $_POST['player'], $_POST['player_name']);
	}
}

?>