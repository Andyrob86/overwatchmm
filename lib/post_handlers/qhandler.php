<?php
$root = ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false)?"{$_SERVER['DOCUMENT_ROOT']}/overwatchmm/www":$_SERVER['DOCUMENT_ROOT'];
include "{$root}/../vendor/autoload.php";
set_debug();
use Respect\Validation\Validator as v;
if ( is_session_started() === FALSE ) {
	$session = new session();
	$session->start_session('_s', true);
}
if (isset($_POST['gamer_tag']) && isset($_POST['platform']) && isset($_POST['region'])){
	$data = array();
	if (v::alnum("#-_")->length(1,18)->validate($_POST['gamer_tag'])){
		$inf = option_array($_POST['region'], $_POST['platform'], $_POST['gamer_tag'], $_POST['lead'], $_POST['game_type'], $_POST['pr'], $_POST['sr'], $_POST['have_mic'], $_POST['mic_req']);
		if (isset($_SESSION['data']['pt'])){
			$inf['pt'] = $_SESSION['data']['pt'];
		} else {
			$factory = new RandomLib\Factory;
			$generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::MEDIUM));
			$inf['pt'] = $generator->generateString(64, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
		}
		if ($inf['gt'] != "badtag"){
			$inf['queries'] = queries($inf['reg'],$inf['plat'],$inf['pr'],$inf['sr'],$inf['have_mic'],$inf['mic_req'],$inf['type']);
			$db = new Database();
			$q = "SELECT `id` from `groups_{$inf['reg']}_{$inf['plat']}` LIMIT 1";
			$db->query($q);
			$db->execute();
			if ($db->rowcount()==0){
				if ($inf['lead'] == 0){
					$data['message'] = offer_lead(2);
					$data['state'] = "error";
					echo json_encode($data);
				};
				if ($inf['lead'] == 1) {
					
					create_group($inf);
				};
			} else {
				
				find_empty_group($inf);
			}
		} else {
			if ($inf['plat'] == "pc"){
				$data['message'] = "<p>Make sure you enter a valid Battle.net name, including your identifier (ex. yourname#1234).</p>";
				$data['state'] = 'error';
				echo json_encode($data);
			}
			if ($inf['plat'] == "xbox"){
				$data['message'] = "<p>Make sure you enter a valid Xbox Live Gamertag.</p>";
				$data['state'] = 'error';
				echo json_encode($data);
			}
			if ($inf['plat'] == "ps4"){
				$data['message'] = "<p>Make sure you enter a valid Playstation Network name.</p>";
				$data['state'] = 'error';
				echo json_encode($data);
			}
		}
	} else {
		$data['message'] = "<p>Please make sure you have entered a valid gamer tag.</p>";
		$data['state'] = 'error';
		echo json_encode($data);
	}
	
};

?>